// java script object that is sent to the server to process user selection

var copyRecordConditions = {
  	    "sourceDB" : "",
          "source" : "",
        "targetDB" : "",
          "target" : "",
		  "config" : [],
 "newRowsUpdtCond" : [],
};

var updateConditions = {
        "targetDB" : "",
          "target" : "",
		  "config" : [],
	   "outdtCond" : []
};

// global variables being used in the program, caching all the details from the ajax calls so that they could be used later in the program
sourcedb = "";targetdb = "";sTable = "";tTable="";
var selectColumns=$('<select>').append($('<option>').val('Select Table').text('Select Table'));
var srcTblColumns = $('<select>');
var alertMsg = "";
var selColumnsTrgtTbl=$('<select>').append($('<option>').val('Select Table').text('Select Table'));


$(document).ready(function(){
	
// builds the javascript object with all the necessary details that needs to be sent to the server	
function getConditions(){
	copyRecordConditions.config.length=0;
	
	copyRecordConditions.sourceDB = sourcedb;
	copyRecordConditions.targetDB = targetdb;
	copyRecordConditions.source = sTable;
	copyRecordConditions.target = tTable;
	copyRecordConditions.newRowsUpdtCond.length=0;

	$('#conditionsTab').find('tr').each(function () {
	    row = $(this);
	    copyRecordConditions.config.push({
	    		"columnName" : row.find(".rcolumns option:selected").val(),
	    		"condition" : row.find(".rule option:selected").val(),
	    		"conditionValue" : row.find(".rValue input").val()
	    })
    });
	
	$('#srcDefaultColumnsTab').find('tr').each(function () {
	    row = $(this);
	    copyRecordConditions.newRowsUpdtCond.push({
	    		"columnName" : row.find(".colName option:selected").val(),
	    		"condition" : "=",
	    		"conditionValue" : row.find(".colValue input").val()
	    })
	});
	//console.log(JSON.stringify(copyRecordConditions));
}
function getCondtionsForUpdate(condLocation){

	updateConditions.config.length=0;
	updateConditions.targetDB = targetdb;
	updateConditions.target = tTable;
	updateConditions.outdtCond.length = 0;
	
	$(condLocation).find('tr').each(function () {
	    row = $(this);
	    updateConditions.config.push({
	    		"columnName" : row.find(".rcolumns option:selected").val(),
	    		"condition" : row.find(".rule option:selected").val(),
	    		"conditionValue" : row.find(".rValue input").val()
	    })
	});   
	    
    $('#trgtDefaultColumnsTab').find('tr').each(function () {
	    row = $(this);
	    updateConditions.outdtCond.push({
	    		"columnName" : row.find(".colName option:selected").val(),
	    		"condition" : "=",
	    		"conditionValue" : row.find(".colValue input").val()
	    })
    });
}

// verifies input provided by the user, other wise alerts 
function validateConfig()
{
	
	if(targetdb=="")
	{
		alertMsg = "select Target DATABASE";
		return false;
	}
	else if(tTable=="")
	{
		alertMsg = "select Target Table";
		return false;
	}
	return true;
}
function validateUpdateConfig()
{
    if(updateConditions.config.length==0 || updateConditions.outdtCond.length == 0)
    {	
	    alertMsg = "select conditions for update";
	    return false;
    }
    else 
        return true;
}

// returns a condition row that gets added to the src as well trgt table based on columns passed
function addCondtionsRow(columns){
	tblColumns = columns;
	var conditions = [ ">", ">=", "<=", "<", "=", "!=","Like","is not null","is null","between"];
	
	var selOption = $('<select>');
	$.each(conditions, function(item, value) {
		selOption.append($("<option>").attr('value', value)
				.text(value));
	});
	
	var textBox = $('<input>').attr("type" ,"text");
									 
	var remButton = $('<input>').attr({ type:"button", 
										value:"-" });
	$(remButton).addClass("delete");
	$(remButton).css('color','#FF0000');
				
	var tr = $('<tr>').attr("class", "row");
	var td1 = $('<td>').attr("class", "rcolumns");
	var td2 = $('<td>').attr("class", "rule");
	var td3 = $('<td>').attr("class","rValue");
			  // td3.attr("width","10%");
	var td4 = $('<td>').attr("class", "remButton");
				
	$(tblColumns).clone().appendTo(td1);
	td2.append(selOption);
	td3.append(textBox);
	td4.append(remButton);
	tr.append(td1,td2,td3,td4);

	return tr;
}

//this function returns a default values row that gets added to the table, it can be called both my source as well target
// takes columns as parameter to build the table row 
function getSetColValTableRow(columns){
	
	tblColumns = columns;
	
	var textBox = $('<input>').attr("type", "text");
	var remButton = $('<input>').attr({
										type:"button", 
										value:"-" });
	$(remButton).addClass("delete");
	$(remButton).css('color','#FF0000');
	
				
	var tr = $('<tr>').attr("class", "row");
	var td1 = $('<td>').attr("class", "colName");
	var td2 = $('<td>').attr("class", "colValue");
	var td3 = $('<td>').attr("class", "remButton");
				
	$(tblColumns).clone().appendTo(td1);
	td2.append(textBox);
	td3.append(remButton);
	tr.append(td1,td2,td3);

	return tr;
}


// handler for copy button click, calls validate config to make sure user selected all the form vales
$("#copyButton").click(function() {
	$("#copyResponse").empty(); 
	getConditions();
	if(validateConfig()){
		$.ajax({
				url : "spring/copyController",
				type : "POST",
				cache : false,
				data : {
					jsonString : JSON.stringify(copyRecordConditions),
				},
				// if received a response of any kind from the server
				success : function(responseText) {
					if(responseText.message === "ok")
						$("#copyResponse").html("Succesfully copied "+responseText.targetRows+" records");
					else
					{
						if(responseText.length == 0)
							$("#copyResponse").html(" Error in conditions or set Default Values ");
						else 
							$("#copyResponse").html(responseText.message);
					}	
				},
				// if not received any response from the server
				error : function() {
					    $("#copyResponse").html("Error processing request, Check all the conditions");
				},
				beforeSend : function() {
					    $('#copyResponse').html('<img src="css/loading.gif"> loading...');
		 			    $("#updateButton").attr('disabled','disabled');
		 			    $("#copyButton").attr('disabled','disabled');
		 			    $('#sourceRowsMsg').empty();
		 			    $('#targetRowsMsg').empty();
			    },
	            complete : function() {
		 			    $("#updateButton").removeAttr("disabled");
		 			    $("#copyButton").removeAttr("disabled");
	            }
	    });
	}
	else
		alert(alertMsg);
});

// handler for update button 
$("#updateButton").click(function() {
	getCondtionsForUpdate('#trgtConditionsTab');
	if(validateConfig() && validateUpdateConfig()){
		$.ajax({
				url : "spring/updateController",
				type : "POST",
				cache : false,
				data : {
					jsonString : JSON.stringify(updateConditions),
				},
				// if received a response of any kind from the server
				success : function(responseText) {
					console.log("response from updateRecords method = "+responseText);
					if(responseText.message === "ok")
						$("#copyResponse").html("Succesfully Updated "+responseText.rowsUpdated+" records");
					else
						$("#copyResponse").html(responseText.message);
				},
				// if not received any response from the server
				error : function() {
					$("#copyResponse").html("no response from server");
				},
				beforeSend : function() {
					$('#copyResponse').html('<img src="css/loading.gif"> loading...');
		 			$("#updateButton").attr('disabled','disabled');
		 			$("#copyButton").attr('disabled','disabled');
		 			$('#sourceRowsMsg').empty();
		 			$('#targetRowsMsg').empty();

			    },
	            complete : function() {
	            	    $("#updateButton").removeAttr("disabled");
			 			$("#copyButton").removeAttr("disabled");
			    },
	    });
	}
	else
		alert(alertMsg);
});
	
// selecting a db in the source section activates this handler
$("#selectSourceDb").change(function() 
{
  sourcedb = $(this).val();
  var db = $(this).val();
  var sourceTablesList="";
  var ajaxUrl = "";
  
  if(db == "localDb")
     tablesUrl = "spring/getLocalTables";
  else if(db == "remoteDb")
	  tablesUrl = "spring/getRemoteTables";
  else if(db == "sqlServerDb")
	  tablesUrl = "spring/getSqlServerTables";
  
  $.ajax({
		url : tablesUrl,
		type : "GET",
		cache : false,
		// if received a response of any kind from the server
		success : function(responseText) {
			var sourceTablesList = responseText;
			console.log(sourceTablesList);
			var selOptions = $('<select>');
			$("#selSourceTable").empty();
			$.each(responseText, function(item,value) {
				$("#selSourceTable").append($("<option>").attr('value',value).text(value));
		    });
		 },
		 // if not received any response from the server
		 error : function() {
				alert("Could Not load Tables Internal error");
		 },
		 beforeSend : function() {
				$('.sourceLoading').html('<img src="css/loading.gif"> loading...');
				$('#sourceRowsMsg').empty();
	 			$('#targetRowsMsg').empty();
	 			$('#deletedRowsMsg').empty();
	 			$('#copyResponse').empty();
		 },
         complete : function() {
		 			$('.sourceLoading').empty();
		 },
	});
});

// selecting a table from the source section activates this function
$("#selSourceTable").change(function()
{
	sTable = $(this).val();
	var columnsUrl="";
	var db = $("#selectSourceDb").val();
	
	if(db == "localDb")
		columnsUrl = "spring/getLocalColumns";
	else if(db == "remoteDb")
		columnsUrl = "spring/getRemoteColumns";
	else if(db == "sqlServerDb")
		columnsUrl = "spring/getSqlServerColumns";
	
    var table = $(this).val();
    $.ajax({
			url : columnsUrl,
			cache : false,
			dataType: "json",
			type: "POST",
			data : {
				tableName : table,
			},
			// if received response of any kind from the server
			success : function(responseText) {
				var sel = $('<select>');
 				var json = responseText;
 				$.each(json, function(item,value) {
 					sel.append($("<option>").attr('value',value).text(value));
 				      });
 				//$('#conditionsTab').find('tr')
 				$("#conditionsTab").find(".rcolumns").empty().append(sel);
 				selectColumns = sel;
			 },
			 // if not received any response from the server
			 error : function() {
					alert("Could Not load Tables Internal error");
			 },
			 beforeSend : function() {
					$('.sourceLoading').html('<img src="css/loading.gif"> loading...');
					$('#sourceRowsMsg').empty();
		 			$('#targetRowsMsg').empty();
		 			$('#copyResponse').empty();
			 },
	         complete : function() {
			 		$('.sourceLoading').empty();
			 },
		});
    });

    // selecting a db in the target section activates this section
    $("#selectTargetDb").change(function() 
		{
	      targetdb = $(this).val();
		  var db = $(this).val();
		  var ajaxUrl = "";
		  
		  if(db == "localDb")
		     tablesUrl = "spring/getLocalTables";
		  else if(db == "remoteDb")
			 tablesUrl = "spring/getRemoteTables"
   	      else if(db == "sqlServerDb")
			 tablesUrl = "spring/getSqlServerTables";
		  
		  $.ajax({
				url : tablesUrl,
				type : "GET",
				cache : false,
				// if received a response of any kind from the server
				success : function(responseText) {
					var targetTablesList = responseText;
					var selOptions = $('<select>');
					$("#selTargetTable").empty();
					$.each(responseText, function(item,value) {
						$("#selTargetTable").append($("<option>").attr('value',value).text(value));
				    });
				 },
				 // if not received any response from the server
				 error : function() {
						alert("Could Not load Tables Internal error");
				 },
				 beforeSend : function() {
						$('.targetLoading').html('<img src="css/loading.gif"> loading...');
						$('#sourceRowsMsg').empty();
			 			$('#targetRowsMsg').empty();
			 			$('#copyResponse').empty();
				 },
		         complete : function() {
				 			$('.targetLoading').empty();
				 },
			});
		});

        // selecting target table activates this handler
		$("#selTargetTable").change(function()
		{
			tTable = $(this).val();
			var columnsUrl="";
			var db = $("#selectTargetDb").val();
			if(db == "localDb")
				columnsUrl = "spring/getLocalColumns";
		    else if(db == "remoteDb")
				columnsUrl = "spring/getRemoteColumns"
			else if(db == "sqlServerDb")
				columnsUrl = "spring/getSqlServerColumns";		
			
		    var table = $(this).val();
		    $.ajax({
					url : columnsUrl,
					cache : false,
					dataType: "json",
					type: "POST",
					data : {
						tableName : table,
					},
					// if received a response of any kind from the server
					success : function(responseText) {
						var sel = $('<select>');
		 				var json = responseText;
		 				$.each(json, function(item,value) {
		 					sel.append($("<option>").attr('value',value).text(value));
		 				      });
		 				$("#trgtConditionsTab").find(".rcolumns").empty().append(sel);
		 				selColumnsTrgtTbl = sel;

					 },
					 // if not received any response from the server
					 error : function() {
							alert("Could Not load Tables Internal error");
					 },
					 beforeSend : function() {
							$('.targetLoading').html('<img src="css/loading.gif"> loading...');
							$('#sourceRowsMsg').empty();
				 			$('#targetRowsMsg').empty();
				 			$('#copyResponse').empty();
					 },
			         complete : function() {
					 			$('.targetLoading').empty();
					 },
				});
		});
		

		// adds conditions row in the source section, calls addContionsRow
		$("#adSrcRow").click(function() {
			srcTblColumns = selectColumns;
			$("#conditionsTab").append(addCondtionsRow(srcTblColumns));
		});
		
		// adds conditions row in the target section, calls addConditionRow function
		$("#adTrgtRow").click(function() {
			trgtTblColumns = selColumnsTrgtTbl;
			$("#trgtConditionsTab").append(addCondtionsRow(trgtTblColumns));
		});

		// deletes condition row from source 
		$('#conditionsTab').on('click', 'td input.delete', function () {
			$(this).closest('tr').remove();
		});
 
		// deletes condition row from target 
		$('#trgtConditionsTab').on('click', 'td input.delete', function () {
			$(this).closest('tr').remove();
		});
		
		// deletes row from target table update values
		$('#trgtDefaultColumnsTab').on('click', 'td input.delete', function () {
			$(this).closest('tr').remove();
		});
		
		// deletes default values row from source
		$('#srcDefaultColumnsTab').on('click', 'td input.delete', function () {
			$(this).closest('tr').remove();
		});
		
		// adds a row to set default values in the source section, calls getSetColValTableRow function
		$("#defColValues").click(function() {

			srcTblColumns = selectColumns;
			$("#srcDefaultColumnsTab").append(getSetColValTableRow(srcTblColumns));
		});
		
        // adds a row to set update values in the target setion, calls getSetColValTableRow function	
		$("#upColValues").click(function() {
			
			trgtTblUpdtColumns = selColumnsTrgtTbl;
			$("#trgtDefaultColumnsTab").append(getSetColValTableRow(trgtTblUpdtColumns));
		});
});