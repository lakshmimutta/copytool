
var ruleSelected = {
   	   "ruleName" : "",
		 "source" : "",
		 "target" : "",
    "delTrgtRows" : "",
 	  "outdtCond" : [],
"newRowsUpdtCond" : [],
	  	 "config" : []
};

var ruleToSave = {
	   "ruleName" : "",
		 "source" : "",
		 "target" : "",
    "delTrgtRows" : "",
	  "outdtCond" : [],
"newRowsUpdtCond" : [],
	   	 "config" : []
}
function updateRule()
{

}

function getUpList(updateRules)
{
	var $table = $("<table>");
    $table.addClass("uprulesClass");
    $table.css({
    	        width:"60%",
    	        "border-width":"1px 1px 1px 1px",
                "border-style": "solid"});

	var condList = $.parseJSON(updateRules);

	if(condList!=null){
	for ( var i = 0, l = condList.length; i < l; i++ ) {
		
		if(i==0)
		{
			var colHeader = $( "<tr>" );
			var th1= $( "<th>" ),th2= $( "<th>" );
			$(th1).html("Column Name");
			$(th1).css("text-align","left");
			$(th2).html("Update Value");
			$(th2).css("text-align","left");

			$(colHeader.append(th1));
			$(colHeader.append(th2));
			
			$table.append(colHeader);
		}	
		var cond = condList[i];
		var $line = $( "<tr>" );
		 $line.append( $( "<td>" ).html(cond.columnName));
	 	 
		var textBox = $('<input>').attr({
										type: "text",
										value: cond.conditionValue });
		var td = $('<td>').attr("class", "updateVal");
		td.append(textBox);
		$line.append(td);
	     
	 
	 $table.append( $line );
      };
    }  
     return $table;
}

function getList(conditionsList)
{
	var $table = $("<table>");
	    $table.addClass("rulesClass");
	    $table.css({
	        width:"80%",
	        "border-width":"1px 1px 1px 1px",
            "border-style": "solid"});
	    
		var condList = $.parseJSON( conditionsList);
            console.log(conditionsList);
		    if(condList!=null)
		    {
		    	$("#mheader").hide();
		    	$("#uheader").show();
		    	$("#rulesDiv").show();
			    for ( var i = 0, l = condList.length; i < l; i++ ) {
				
				if(i==0)
				{
					var colHeader = $( "<tr></tr>" );
					var th1= $( "<th>" ),th2= $( "<th>" ),th3= $( "<th>" );
					$(th1).html("Column Name");
					$(th1).css("text-align","left");
					$(th2).html("Condition");
					$(th2).css("text-align","left");
					$(th3).html("Condition Value");
					$(th3).css("text-align","left");

					$(colHeader.append(th1));
					$(colHeader.append(th2));
					$(colHeader.append(th3));
					$table.append(colHeader);
				}	
				var cond = condList[i];
				var $line = $( "<tr>" );
                 
				td1 = $('<td>').attr("class", "td1Class");
                 $(td1).html(cond.columnName);
                 
				 td2 = $('<td>').attr("class", "td2Class");
                 $(td2).html(cond.condition);
                 
			     td3 = $('<td>').attr("class", "editBox");
			 
			 if(cond.ise == true)
			 {	 
				var textBox = $('<input>').attr({
												type: "text",
												value: cond.conditionValue });
				td3.append(textBox);
			 }    
			 else 
				$(td3).html(cond.conditionValue);
			    
			 $line.append( td1);
			 $line.append(td2);
			 $line.append(td3);
			 $table.append( $line );
		     }
		}
	 return $table;
  }

function search(index, myArray){
    for (var i=0; i < myArray.length; i++) {
        if (i == index) {
            return myArray[i];
        }
    }
}

function updateEditedRules()
{
	
	ruleToSave.ruleName = ruleSelected.ruleName;
	ruleToSave.source = ruleSelected.source;
	ruleToSave.target = ruleSelected.target;
	ruleToSave.delTrgtRows = ruleSelected.delTrgtRows;
	ruleToSave.newRowsUpdtCond.length = 0;
	ruleToSave.outdtCond.length = 0;
    ruleToSave.config.length = 0;
	var rowcount = 0;
	
	//get all the conditions from the form
	$('#conditions').find('tr').each(function () {
	    row = $(this);
	    if(rowcount>0)
	    {	
	        var cond = search(rowcount-1, ruleSelected.config);
	    	console.log(cond);
	        var editedVal = cond.conditionValue;
	    	
	        if(cond.ise==true)
	        	editedVal = row.find(".editBox input").val();
	    	if(cond.ism==true && (editedVal==null || editedVal == ""))
	    	{
	    		editedVal = row.find(".editBox input").val();
	    		alert("Mandatory fields cannot be empty");
	    		throw new Error("Mandatory fields cannot be empty");
	    	}
	    
 	    ruleToSave.config.push({
	    		"columnName" : cond.columnName,
	    		"condition" : cond.condition,
	    		"conditionValue" : editedVal,
	    		"ism" : cond.ism,
	    		"ise" : cond.ise
	    });
	  }  
	 rowcount++;   
    });
	
	//get all the update rules from the form
	var uprowCount=0
	$("#upConditions").find('tr').each(function () {
		row = $(this);
	    if(uprowCount>0)
	    {	
	        var cond = search(uprowCount-1, ruleSelected.outdtCond);
	    	console.log(cond);
	       
	    	ruleToSave.outdtCond.push({
	    		"columnName" : cond.columnName,
	    		"condition" : cond.condition,
	    		"conditionValue" : row.find(".updateVal input").val()
	    });
	  }  
	  uprowCount++; 
	});
	//get all the new records update rules from the form
	var inrowCount=0;
	$("#inConditions").find('tr').each(function () {
		row = $(this);
	    if(inrowCount>0)
	    {	
	        var cond = search(inrowCount-1, ruleSelected.newRowsUpdtCond);
	    	console.log(cond);
	       
	    	ruleToSave.newRowsUpdtCond.push({
	    		"columnName" : cond.columnName,
	    		"condition" : cond.condition,
	    		"conditionValue" : row.find(".updateVal input").val()
	        });
	    }  
	  inrowCount++;
	});
}
$(document).ready(function() {
		$("#copyButton").click(function() {
			console.log(JSON.stringify(ruleSelected));
				if(ruleSelected.ruleName==="")
				    alert("please select A rule");
				else
				{
					$('#copyResponse').html('<img src="css/loading.gif"> loading...');
					$.ajax({
							url : "spring/ajax2Servlet",
							type : "POST",
							cache : false,
							contentType : "application/x-www-form-urlencoded; charset=UTF-8;",
							data : {
								jsonString : JSON.stringify(ruleSelected),
							},
							// if received a response of any kind from the server
							success : function(responseText) {
								$("#copyResponse").html(responseText);
							},
							// if not received any response from the server
							error : function() {
								$("#copyResponse").html("no response from server");
							}
					  });
    			}
	     });
		$('#conditions').on('click', 'table.rulesClass', function () {
			    $("#editedRule").show();
		 });
		$("#upConditions").on('click', 'table.uprulesClass', function () {
		    $("#editedRule").show();
		 });
		$("#inConditions").on('click', 'table.uprulesClass', function () {
		    $("#editedRule").show();
		 });
		
		$('#editedRule').click(function(){
			updateEditedRules();
			console.log(ruleToSave);
			$("#copyResponse").empty();
			
			$('#copyResponse').html('<img src="css/loading.gif"> loading...');
				
			$.ajax({
					url : "spring/ajax2Servlet",
					type : "POST",
					cache : false,
					contentType : "application/x-www-form-urlencoded; charset=UTF-8;",
					data : {
						jsonString : JSON.stringify(ruleToSave),
						from : "update",
					},
					// if received a response of any kind from the server
					success : function(responseText) {
						$("#copyResponse").html(responseText);
					},
					// if not received any response from the server
					error : function() {
						$("#copyResponse").html("No response from server");
					}
  			});
		});

		$.ajax({
			url : "spring/ajaxServlet",
			type : "GET",
			cache : false,
			// if received a response of any kind from the server
			success : function(responseText) {
				var jsonText = responseText;
				console.log(jsonText);
				$("#tree").fancytree({
					  extensions: ["filter"],
					  source: [jsonText],
					  activate: function(event, data){
			          },
					  filter: {
					        autoApply: true,
					        // autoExpand: true,
					        mode: "hide"
					      },
				      click:function(event, data){
	                      
				    	     $("#copyResponse").empty();
				    	     $('#editedRule').hide();
				    	     var node = data.node;
						     if(!node.isFolder()){
						    	 $('.upClass').show();
						         ruleSelected.ruleName = node.title;
						    	 ruleSelected.source = node.data.sourceTable;
						    	 ruleSelected.target = node.data.targetTable;
						    	 ruleSelected.config = jQuery.parseJSON(node.data.ruleList);
						    	 ruleSelected.delTrgtRows = node.data.deleteRows;
						    	 ruleSelected.outdtCond = jQuery.parseJSON(node.data.upRules);
						    	 ruleSelected.newRowsUpdtCond = jQuery.parseJSON(node.data.newRowsUpRules);
						    	 console.log(ruleSelected);
						     $("#title").html(node.title);
						     $("#sourc").html(node.data.sourceTable);
						     $("#trgt").html(node.data.targetTable);
						     $("#conditions").empty().append(getList(node.data.ruleList)) ;
						     $("#upConditions").empty().append(getUpList(node.data.upRules));
						     $("#inConditions").empty().append(getUpList(node.data.newRowsUpRules));
						     } 
						   }   
					    });    
				var tree = $("#tree").fancytree("getTree");
				$("input[name=search]").keyup(function(e){
				      var n,        
				        match = $(this).val();

				      if(e && e.which === $.ui.keyCode.ESCAPE || $.trim(match) === ""){
				        $("button#btnResetSearch").click();
				        return;
				      }
				          // Pass a string to perform case insensitive matching
				          n = tree.filterNodes(match);
				    
				      $("button#btnResetSearch").attr("disabled", false);
				      $("span#matches").text("(" + n + " matches)");
				    }).focus();

				    $("button#btnResetSearch").click(function(e){
				      $("input[name=search]").val("");
				      $("span#matches").text("");
				      tree.clearFilter();
				    }).attr("disabled", true);
			},
			// if not received any response from the server
			error : function() {
				$("#tree").html("Could Not load rules Internal error");
  		}
	});
});