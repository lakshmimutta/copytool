var targetTable="";

$(document).ready(function() {
	
	$.ajax({
			url : "spring/getTree2Data",
			type : "GET",
			cache : false,
			// if received a response of any kind from the server
			success : function(responseText) {
				var jsonText = responseText;
				$("#tree2").fancytree({
					  extensions: ["filter"],
					  source: [jsonText],
					  activate: function(event, data){
			          },
			          filter: {
					      autoApply: true,
					      mode: "hide"
					  },
					  click:function(event, data){
						  $("#copyRecResponse").empty(); 
							$("#savRuleResponse").empty();
						  var node = data.node;
					       targetTable = data.node.title;
					  }     
					});
				var tree2 = $("#tree2").fancytree("getTree");
				$("input[name=search2]").keyup(function(e){
				      var n,        
				        match = $(this).val();

				      if(e && e.which === $.ui.keyCode.ESCAPE || $.trim(match) === ""){
				        $("button#btnResetSearch2").click();
				        return;
				      }
				          // Pass a string to perform case insensitive matching
				          n = tree2.filterNodes(match);
				    
				      $("button#btnResetSearch2").attr("disabled", false);
				      $("span#matches2").text("(" + n + " matches)");
				    }).focus();

				    $("button#btnResetSearch2").click(function(e){
				      $("input[name=search2]").val("");
				      $("span#matches2").text("");
				      tree2.clearFilter();
				    }).attr("disabled", true);
			},
			// if not received any response from the server
			error : function() {
				                $("#tree2").html("Could Not load Tables Internal error");
								},
			beforeSend : function() {
								$('.hidden').html('<img src="css/loading.gif"> loading...');
								},
			complete : function() {
									$('.hidden').empty();
								},
	});
});
