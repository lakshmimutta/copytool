<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>

<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Copy Records Selection Page</title>
<script src="js/jquery.js" type="text/javascript"></script>
<script src="js/jquerymin.js" type="text/javascript"></script>
<script src="js/jquery-ui.custom.js" type="text/javascript"></script>
<script src="js/jquery-ui.js" type="text/javascript"></script>
<script src="js/jquery.smartmenus.js" type="text/javascript"></script>
<script src="js/jquery.corner.js" type="text/javascript"></script>
<script src="js/copyrecordsnew.js" type="text/javascript"></script>

<link href='css/sm-core-css.css' rel='stylesheet' type='text/css'/>
<link href='css/sm-blue.css' rel='stylesheet' type='text/css' />
<link href="css/jquery-ui.min.css" rel="stylesheet" type="text/css">
<!-- <link href="css/makerulediv.css" rel="stylesheet" type="text/css"> -->

<script>
	$(function() {
		$('#main-menu').smartmenus();
	});
	$(function(){
        $('#fromDiv').corner();
        $('#toDiv').corner();
        $('.seperator').corner();
	});
</script>

</head>
<body>
	<form>
		<div id = "main" style="width: 100%; position:relative;">
    	  <div id="header" style="height: 100px; width: 100%; position:absolute;">
	        <div style= "float: right;">
	        	<br><B> Copy Tool  </B><br>
	        	<B> User: </B> ${userName}<br>
	        	<B> Date: </B> <%=new java.util.Date()%>
	        </div>
	      </div>
		<div id="menus" style="width: 100%;height: 100px; top: 100px; position:absolute;">
			<ul id="main-menu" class="sm sm-blue">
				<li><a href="main.htm">Index Page</a></li>
				<li><a href="#">Rule Builder</a>
					<ul>
					    <li><a href="alltables.htm"> Rule Builder Page </a> <li>
					</ul>
				</li>
				<li><a href="#">Rule Executor</a>
					<ul>
						<li><a href="copytables.htm">Rule Executor Page</a></li>
					</ul>
				</li>
				<li><a href="#">Bunch Copy</a>
					<ul>
						<li><a href="copyrecordsnew.htm">Copy Recods</a> <li>
					</ul>
				</li>
				<li><a href="logout.do">User Logout</a></li>
				
			</ul>
		</div>
		<div id="sectiontitle" style="height: 50px; top: 150px; position:absolute;">
		    <div style="margin-left: 520px;">
              <h3 align="center">Bunch Copy Distributions</h3>
              	<div align="center" class="sourceLoading"></div>
              	<div align="center" class="targetLoading"></div>
              	<div align="center" >
              	    <B id="copyResponse"></B>
              	</div>
            </div>		
		</div>
		<div id="container" style="width: 100%; height: 500px; position: absolute; top: 220px;"> 
			<div id="fromDiv" style="background-color:#F0F0F0;margin-top: 20px; width: 35%; float: left; border:1px solid black; height: 350px; margin-bottom: 20px;margin-right: 15px;	margin-left: 130px;">
		      <div class="seperator" data-corner="top 5px" style="background-color:#B8B8B8;height: 35px; border :1px solid grey;">   
			   	<B style="margin-left: 10px; margin-top:15px;"> FROM </B>
			  </div>
			  <table id="fromDisplayTable" style="width:100%; border:0px;font-weight: bold; ">
  				<tr class="tableRow">
    			  <td width = "31%" class="dbCell" align="right">DataBase:</td>
                  <td width = "69%" class="dbSelect">
                     <select id="selectSourceDb" style ="max-width: 100px; width: 100%;">
                        <option value="localDb">Local</option>
                        <option value="remoteDb">Remote</option>
                        <option value="sqlServerDb">SqlServer</option>
                        <option value="selectDb" selected="selected">Select DB</option>
                     </select>
                  </td>
                </tr>
                <tr class="tableRow">
                  <td width = "31%" class="tableCell" align="right">Table:</td>
                  <td width = "69%" class="tableSelect" >
                      <select id="selSourceTable">
                         <option value="fromTable">
                           Select Table 
                         </option>
                      </select>
                  </td>		
                </tr>
             </table>
           <table style="width:100%; border:0px;font-weight: bold; ">
             <tr>
                <td width = "31%" align="right">Query Conditions:</td>  
           		<td width = "69%">
           			<input type="button" id="adSrcRow" value="Add Conditions" /> 
           		</td>
             </tr>
           </table>  
           <table id="conditionsTab" style="width: 100%;">

 	       </table>
 	       <table style="width:100%; border:0px;font-weight: bold; ">
 	        <tr>
 	          <td width = "31%" align="right">
           		Set Default Values:
              </td>
              <td width = "69%">
                <input type="button" id="defColValues" value="SetColumnDefaults" />
              </td>
            </tr>
           </table> 	       
           <table id="srcDefaultColumnsTab" style="width: 78%;">

 	       </table>
 	       
 	       <br>
 	       <div style="margin-left: 32%;">   
 	          <B id="sourceRowsMsg"> </B>
 	       </div>
		   </div>
		
		   <div id="toDiv" style="background:#F0F0F0; width:35%;float: right;border:1px solid black;height: 350px; margin-top: 20px; margin-bottom: 20px; margin-right: 130px; margin-left: 15px;">
		   <div class="seperator" data-corner="top 5px" style="background-color:#B8B8B8;height: 35px; border :1px solid grey;">
   	   		   <B style="margin-left: 10px;margin-top: 15px;"> TO </B>
   	   	   </div>
			  <table id="toDisplayTable" style="width:100%; border:0px;font-weight: bold;">
  						<tr class="tableRow">
    			  <td width = "32%" class="dbCell" align="right">DataBase:</td>
                  <td width = "68%" class="dbSelect">
                     <select id="selectTargetDb" style ="max-width: 100px; width: 100%;">
                        <option value="localDb">Local</option>
                        <option value="remoteDb">Remote</option>
                        <option value="sqlServerDb">SqlServer</option>
                        <option value="selectDb" selected="selected">Select DB</option>
                     </select>
                  </td>
                </tr>
                <tr class="tableRow">
                  <td width = "32%" class="tableCell" align="right">Table:</td>
                  <td width = "68%">
                      <select id="selTargetTable">
                         <option value="toTable">
                           Select Table 
                         </option>
                      </select>
                  </td>
                </tr>
              </table>
              <table style="width:100%; border:0px;font-weight: bold;">
                <tr> 
                  <td width = "32%" align="right"> 
              		Update Conditions:
              	  </td>
                  <td width = "68%">
           			  <input type="button" id="adTrgtRow" value="Add Conditions" />
                  </td>
                </tr>
              </table>
              <table id="trgtConditionsTab" style="width: 100%;">

   	          </table>
   	          <table style="width:100%; border:0px;font-weight: bold;"> 
   	          <tr >
   	          	<td width = "32%" align="right">
   	          		Set Update values:
   	          	</td>
   	          	<td width = "68%"> 
        			<input type="button" id="upColValues" value="SetUpdateValues" />
   	          	</td>
   	          </tr>
   	          </table>
              <table id="trgtDefaultColumnsTab" style="width: 78%;">

 	          </table>
 	          <br><br>
			  <div style ="margin-left: 33%;"> 
			     <input type="button" id="copyButton" value="Copy" />
			     <input type="button" id="updateButton" value="Update" />
			  <br><br>
			  <div style="margin-left: 23%;">   
			     <B id="targetRowsMsg"> </B>
			  </div>
			  <div style="margin-left: 23%;">   
			     <B id="deletedRowsMsg"> </B>
			  </div>
			</div>
	    </div>
	  </div>   
	 </div>
    </form>
</body>
</html>