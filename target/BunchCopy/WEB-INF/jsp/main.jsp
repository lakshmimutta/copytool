<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Copy Tool Index Page</title>

<script src="js/jquerymin.js" type="text/javascript"></script>
<script src="js/jquery.smartmenus.js" type="text/javascript"></script>

<link href='css/sm-core-css.css' rel='stylesheet' type='text/css' />
<link href='css/sm-blue.css' rel='stylesheet' type='text/css' />

<script>
	$(function() {
		$('#main-menu').smartmenus();
	});
</script>

</head>
<body>
	<form>
		<div id="header">
			<ul id="main-menu" class="sm sm-blue">
				<li><a href="#">Index Page</a></li>
				<li><a href="#">Rule Builder</a>
					<ul>
						<li><a href="alltables.htm">Rule Builder Page</a></li>
					</ul>
				</li>
				<li><a href="#">Rule Executor</a>
					<ul>
						<li><a href="copytables.htm">Rule Executor Page</a></li>
					</ul>
				</li>
				    <li><a href="#">Bunch Copy</a>
					<ul>
						<li><a href=copyrecordsnew.htm> Copy Records </a> <li>
					</ul>
				</li>
				<li><a href="logout.do">User Logout</a></li>
			</ul>
		</div>
		<div align="center">  ${message} </div>
		
		<div style="width: 100%; top: 100px; position: absolute;">
			<h1 align="center">DATA COPY TOOL</h1>
			<h2>Contents</h2>
			<table border="1" cellpadding="5" width=400>
				<tr>
					<td><a href="alltables.htm">Rule Builder Page </a></td>
				</tr>
				<tr>
				</tr>
				<tr>
					<td><a href="copytables.htm">Rule Executor Page</a></td>
				</tr>
				<tr>
				</tr>
				<tr>
					<td><a href="copyrecordsnew.htm">Bunch Copy</a></td>
				</tr>
			</table>

		</div>
	</form>
</body>
</html>