<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Copy Records From Source Table To Target Table</title>
<script src="js/jquerymin.js" type="text/javascript"></script>
<script src="js/jquery-ui.custom.js" type="text/javascript"></script>
<script src="js/jquery.fancytree.js" type="text/javascript"></script>
<script src="js/jquery.fancytree.filter.js" type="text/javascript"></script>
<script src="js/copyrecords.js" type="text/javascript"></script>
<script src="js/jquery.smartmenus.js" type="text/javascript"></script>

<link href='css/sm-core-css.css' rel='stylesheet' type='text/css' />
<link href='css/sm-blue.css' rel='stylesheet' type='text/css' />
<link href="css/ui.fancytree.css" rel="stylesheet" type="text/css">
<link href="css/jquery-ui.min.css" rel="stylesheet" type="text/css">

<script>
	$(function() {
		$('#main-menu').smartmenus();
	});
</script>


</head>
<body>
	<form>
		<div id="header">
			<ul id="main-menu" class="sm sm-blue">
				<li><a href="main.htm">Index Page</a></li>
				<li><a href="#">Rule Builder</a>
					<ul>
						<li><a href="alltables.htm"> Rule Builder tree structure </a></li>
					</ul>
				</li>
				<li><a href="#">Rule Executor</a>
					<ul>
						<li><a href="copytables.htm">Rule Executor Page</a></li>
					</ul>
				</li>
				<li><a href="#">Bunch Copy</a>
					<ul>
						<li><a href=copyrecordsnew.htm> Copy Records</a> <li>
					</ul>
				</li>
				<li><a href="logout.htm">User Logout</a></li>
			</ul>
		</div>
		<div style="width: 100%; top: 100px; position: absolute;">
			<div
				style="display: inline-block; float: left; width: 30%; padding-right: 5%; vertical-align: top;">
				<p>
					<label>Filter:</label> <input name="search" placeholder="Filter...">
					<button id="btnResetSearch">&times;</button>
					<span id="matches"></span>
				</p>

				<fieldset>
					<legend>Tables At Source</legend>
					<div id="tree"></div>
				</fieldset>
			</div>
			<div
				style="display: inline-block; float: left; width: 60%; padding-right: 5%; vertical-align: top;">

				<h2 id="mheader">Select a rule from the tree for details</h2>
                <h2 id="uheader" style="display: none;"> Update Conditions  </h2>
                <h3 class="upClass" style="display: none;">Old Records </h3>         
                <div id="upConditions" >
                </div>
                <h3 class="upClass" style="display: none;">New Records </h3>
                <div id="inConditions" >
                </div>
 
				<div id="rulesDiv" style="display: none;">
                    <h2 id="rheader"> Rule Details  </h2>
					<div >
						<div style="display: inline-block;"><b> Rule Name :</b></div>
						<div id="title" style="display: inline-block;"> </div>
					</div><br>
					<div>
						<div style="display: inline-block;"><b>Source Table :</b></div>
						<div id="sourc" style="display: inline-block;"> </div>
					</div><br>
					<div>
						<div style="display: inline-block;"><b>Target Table :</b></div>
						<div id="trgt" style="display: inline-block;"></div>
					</div><br>
					<div>
						<div style="display: inline-block; vertical-align: top;"><b>Rule Conditions :</b></div>
						<div id="conditions" style="display: inline-block vertical-align: top;"></div>
					</div><br>
					<div>
						<input type="button" id="copyButton" value="Execute Selected Rule" />
						<input type="button" id="editedRule" value="Execute Edited Rule "
							style="display: none;" />
					</div>
				</div><br>
				
				<div id="copyResponse"></div>
			</div>
		</div>
	</form>
</body>
</html>