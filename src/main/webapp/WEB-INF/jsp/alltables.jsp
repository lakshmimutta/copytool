<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Rule Builder Page</title>

<script src="js/jquerymin.js" type="text/javascript"></script>
<script src="js/jquery.js" type="text/javascript"></script>
<script src="js/jquery-ui.custom.js" type="text/javascript"></script>
<script src="js/jquery.fancytree.js" type="text/javascript"></script>
<script src="js/jquery.fancytree.filter.js" type="text/javascript"></script>
<script src="js/jquery-ui.js" type="text/javascript"></script>
<script src="js/tablesfortree1.js" type="text/javascript"></script>
<script src="js/tablesfortree2.js" type="text/javascript"></script>
<script src="js/buildrules.js" type="text/javascript"></script>
<script src="js/jquery.smartmenus.js" type="text/javascript"></script>

<link href='css/sm-core-css.css' rel='stylesheet' type='text/css' />
<link href='css/sm-blue.css' rel='stylesheet' type='text/css' />
<link href="css/ui.fancytree.css" rel="stylesheet" type="text/css">
<link href="css/jquery-ui.min.css" rel="stylesheet" type="text/css">

<style type="text/css">
.ui-datepicker {
	font-size: 8pt !important
}
</style>
<script>
	$(function() {
		$('#main-menu').smartmenus();
	});
</script>

</head>
<body>
	<form>
		<div id="header">
			<ul id="main-menu" class="sm sm-blue">
				<li><a href="main.htm">Index Page</a></li>
				<li><a href="#">Rule Builder</a>
					<ul>
						<li><a href="alltables.htm">Rule Builder Tree Structure </a></li>
					</ul>
				</li>
				<li><a href="#">Rule Executor</a>
					<ul>
						<li><a href="copytables.htm">Rule Executor Page</a></li>
					</ul>
				</li>
				<li><a href="#">Bunch Copy</a>
					<ul>
						<li><a href=copyrecordsnew.htm> Copy Records</a> <li>
					</ul>
				</li>
				<li><a href="logout.htm">User Logout</a></li>
			</ul>
		</div>
		<div style="width: 100%; top: 100px;">
			<div
				style="display: inline-block; float: left; width: 15%; padding-right: 5%; vertical-align: top;">
				<p>
					<label>Filter:</label> <input name="search" placeholder="Filter...">
					<button id="btnResetSearch">&times;</button>
					<span id="matches"></span>
				</p>
                <div class="hidden1"></div>
				<fieldset>
					<legend>Tables At Source</legend>
					<div id="tree1"></div>
				</fieldset>
			</div>

			<div
				style="display: inline-block; float: left; padding-left: 2%; vertical-align: top; width: 50%;">

				<h2 align="center">Conditions and Rules</h2>

				<h3>Configure Rule...</h3>

				Enter Rule Name: <input type="text" id="ruleName" />
				<br><br> 
				<input type="checkbox" id="delChkBox" class="delOrUpdate">
				Delete rows in target Table 
				<input type="checkbox" id="updateChkBox" 
								class="delOrUpdate">Update old and new Records <br>
				
				<div id="updateConditions" style="display: none;">

					<b>Choose conditions to Outdate old records:</b>
					<input type="button" id="outdtCond" value="Add Outdate Condition">

					<table id="outdateConditions" style="width: 100%;">

					</table>
				</div>
				
				<div id="inputConditions" style="display: none;">
		
					<b>Choose input conditions for new records:</b>
					<input type="button" id="inputCond" value="Add input rows update values">

                    <table id="inputColConditions" style="width: 100%;">

					</table>
				</div>

				<input type="button" id="adRow" value="Add Rule Condition" />

				<table id="conditionsTab" style="width: 100%;">

				</table>

				<h3>Do these actions...</h3>
				<div id="actions"></div>

				<input type="button" id="savRules" value="Save Rule" /> 
				<input type="button" id="copyRecords"
					value="Copy records with the selected rules" />
				<div>

					<h3>Action Response...</h3>
					<br>
					<div id="copyRecResponse"></div>
					<div id="savRuleResponse"></div>
				</div>
			</div>

			<div
				style="display: inline-block; float: right; width: 15%; vertical-align: top; padding-right: 7%;">
				<p>
					<label>Filter:</label> <input name="search2"
						placeholder="Filter...">
					<button id="btnResetSearch2">&times;</button>
					<span id="matches2"></span>
				</p>
				<div class="hidden"></div>

				<fieldset>
					<legend>Tables at Destination</legend>
					<div id="tree2"></div>
				</fieldset>
			</div>
		</div>
	</form>
</body>
</html>










