var copyConditions = {
		"ruleName" : "",
		  "source" : "",
		  "target" : "",
	 "delTrgtRows" : "",
	   "outdtCond" : [],
 "newRowsUpdtCond" : [],
		  "config" : []
};
var newRecVal = {};
function getRules()
{
	copyConditions.ruleName = $("#ruleName").val();
	copyConditions.source = sourceTable;
	copyConditions.target = targetTable;
	copyConditions.config.length = 0;
	copyConditions.outdtCond.length = 0;
	copyConditions.newRowsUpdtCond.length = 0;
	
	
	if (($('#delChkBox').is(":checked")))
	    {
	    	copyConditions.delTrgtRows = "true";
	    }
	else if(($('#updateChkBox').is(":checked")))
	   	{
		    copyConditions.delTrgtRows = "false";
		    
		 $('#outdateConditions').find('tr').each(function(){
		 trow = $(this);
		 copyConditions.outdtCond.push({
			   "columnName" : trow.find(".trgtTblColumn option:selected").val(),
    		   "condition" : trow.find(".options option:selected").val(),
    		   "conditionValue" : trow.find(".orValue input").val()
    		   })
		});
	    $('#inputColConditions').find('tr').each(function(){
		trow = $(this);
		newRecVal[trow.find(".itrgtTblColumn option:selected").val()] =
											trow.find(".irValue input").val();
		copyConditions.newRowsUpdtCond.push({
		      "columnName" : trow.find(".itrgtTblColumn option:selected").val(),
			  "condition" : trow.find(".ioptions option:selected").val(),
			  "conditionValue" : trow.find(".irValue input").val()
		      })
	    });
   	}
	$('#conditionsTab').find('tr').each(function () {
	    row = $(this);
	    	    
	    copyConditions.config.push({
	    		"columnName" : row.find(".srcTblColumn option:selected").val(),
	    		"condition" : row.find(".rule option:selected").val(),
	    		"conditionValue" : row.find(".rValue input").val(),
	    		"ism" : row.find(".ismValue input").prop('checked'),
	    		"ise" : row.find(".iseValue input").prop('checked')
	    })
    });
     
	console.log(JSON.stringify(copyConditions));
	console.log(JSON.stringify(newRecVal));
  }
	
$(document).ready(function() {
			$("#adRow").click(function() {
        				var srcTblColumns = selColumns;
						var conditions = [ "Greater than", "Greater then or Equal to", "Less than or Equal to", "Less Than",
								"equal to", "is not null", "is null","not equal to","Date To From","Like" ];
						
						var selOption = $('<select>');
						$.each(conditions, function(item, value) {
							selOption.append($("<option>").attr('value', value)
									.text(value));
						});
						
						var textBox = $('<input>').attr("type", "text");
						var remButton = $('<input>').attr({
															type:"button", 
															value:"Remove" });
						$(remButton).addClass("delete");
						$(remButton).css('color','#FF0000');
						
						var divIsm = $('<div>');
						$(divIsm).html("ISM");
						var ism = $('<input>').attr({
													  type:"checkbox",
													  value:"ISM"});
						$(ism).addClass("ismClass");
						
						var divIse = $('<div>');
						$(divIse).html("ISE");
						var ise = $('<input>').attr({
							                          type:"checkbox",
						                              value:"ISE"});
						$(ise).addClass("iseClass");
						
						var tr = $('<tr>').attr("class", "row");
						var td1 = $('<td>').attr("class", "srcTblColumn");
						var td2 = $('<td>').attr("class", "rule");
						var td3 = $('<td>').attr("class", "rValue");
						var td4 = $('<td>').attr("class", "ismValue");
						var td5 = $('<td>').attr("class", "ismDiv");
						var td6 = $('<td>').attr("class", "iseValue");
						var td7 = $('<td>').attr("class", "iseDiv");
						var td8 = $('<td>').attr("class", "remButton");
						
						
        				$(srcTblColumns).clone().appendTo(td1);
						td2.append(selOption);
						td3.append(textBox);
						td4.append(ism);
						td5.append(divIsm);
						td6.append(ise);
						td7.append(divIse);
                        td8.append(remButton);
						tr.append(td1, td2, td3,td4,td5,td6,td7,td8);

						$("#conditionsTab").append(tr);

					});
			
			$('#conditionsTab').on('click', 'td input.delete', function () {
				copyConditions.config.length = 0;				
			    $(this).closest('tr').remove();
			});
			
			$("#outdtCond").click(function() {
						
				var trgtTblColumns = selColumns;
				var choices = ["Set DateTo","Active/InActive"];
				
				var selOptions = $('<select>');
				$.each(choices, function(item, value) {
					selOptions.append($("<option>").attr('value', value)
							.text(value));
				});
				$(selOptions).addClass("selOpt");
				
				var input = $('<div>').attr("class","dateOrActive");
				var delButton = $('<input>').attr({
													type:"button", 
													value:"Delete" });
				$(delButton).addClass("del");
				$(delButton).css('color', '#FF0000');

				var tr = $('<tr>').attr("class", "orow");

				var td1 = $('<td>').attr("class", "trgtTblColumn");
				var td2 = $('<td>').attr("class", "options");
				var td3 = $('<td>').attr("class", "orValue");
				var td4 = $('<td>').attr("class", "delButton");

				$(trgtTblColumns).clone().appendTo(td1);
				td2.append(selOptions);
				td3.append(input);
                td4.append(delButton);
				tr.append(td1, td2, td3,td4);

				$("#outdateConditions").append(tr);
			});
			
			$('#outdateConditions').on('click', 'td select.selOpt', function () {
			    
				var dateBox = $('<input>').attr("type","text");
				  $(dateBox).addClass("datePicker");
				  $(dateBox).datepicker({
					  dateFormat: 'yy/mm/dd'
				  });
                var checkBox = $('<input>').attr("type","checkbox");
                    $(checkBox).addClass("isActive");
				var row = $(this).closest('tr');
				
				if($(this).val()=="Set DateTo")
                {					
					row.find(".orValue div").html(dateBox);
                }	
				else if($(this).val()=="Active/InActive")
                {
					row.find(".orValue div").html(checkBox);
                }
			});

			$("#inputCond").click(function() {
							
				var trgtTblColumns = selColumns;
				var choices = ["Set DateTo","Active/InActive"];
				
				var selOptions = $('<select>');
				$.each(choices, function(item, value) {
					selOptions.append($("<option>").attr('value', value)
							.text(value));
				});
				$(selOptions).addClass("seliOpt");
				
				var input = $('<div>').attr("class","idateOrActive");
				var delButton = $('<input>').attr({
													type:"button", 
													value:"Delete" });
				$(delButton).addClass("idel");
				$(delButton).css('color', '#FF0000');

				var tr = $('<tr>').attr("class", "irow");

				var td1 = $('<td>').attr("class", "itrgtTblColumn");
				var td2 = $('<td>').attr("class", "ioptions");
				var td3 = $('<td>').attr("class", "irValue");
				var td4 = $('<td>').attr("class", "idelButton");

				$(trgtTblColumns).clone().appendTo(td1);
				td2.append(selOptions);
				td3.append(input);
                td4.append(delButton);
				tr.append(td1, td2, td3,td4);

				$("#inputColConditions").append(tr);
			});

			
			$('#inputColConditions').on('click', 'td select.seliOpt', function () {
			    
				var idateBox = $('<input>').attr("type","text");
				  $(idateBox).addClass("idatePicker");
				  $(idateBox).datepicker({
					  dateFormat: 'yy/mm/dd'
				  });
                var icheckBox = $('<input>').attr("type","checkbox");
                    $(icheckBox).addClass("isiActive");
				var row = $(this).closest('tr');
				
				if($(this).val()=="Set DateTo")
                {					
					row.find(".irValue div").html(idateBox);
                }	
				else if($(this).val()=="Active/InActive")
                {
					row.find(".irValue div").html(icheckBox);
                }
			});
			
			
			$('#outdateConditions').on('click', 'td input.del', function () {
				//copyConditions.outdtCond.length = 0;				
			    $(this).closest('tr').remove();
			});
			
			$('#inputColConditions').on('click', 'td input.idel', function () {
				//copyConditions.newRowsUpdtCond.length = 0;
			    $(this).closest('tr').remove();
			});
			
			$('input.delOrUpdate').on('change', function() {
			    $('input.delOrUpdate').not(this).prop('checked', false);  
			});
			
			$('#updateChkBox').on('change',function() {

				if(this.checked)
				{	
				    $("#updateConditions").show();
                    $("#inputConditions").show();				    
				}    
				else
				{	
					$("#updateConditions").hide();
					$("#inputConditions").hide();
				}	
			 });    
			
			$('#delChkBox').on('change',function() {
				if(this.checked)
				{	
				    $("#updateConditions").hide();
				    $("#inputConditions").hide();
				}    
			 });    
			
			$("#savRules").click(function() {

				$("#copyRecResponse").empty(); 
				$("#savRuleResponse").empty();
					
					getRules();
					if(copyConditions.ruleName=="" )
					{
						alert("please select a rule name");
						throw new Error("Please select a rule Name");	
					}
					else if(copyConditions.source=="")
					{
						alert("please select the source table");
					    throw new Error("Please select source table");
					}
					else if(copyConditions.target=="")
					{
						alert("please select the target table");
					    throw new Error("Please select target table");
					}
					else if(copyConditions.config.length == 0)
			       		alert("Do not have any Rule Conditions To Save");
					
					
					$('#savRuleResponse').html('<img src="css/loading.gif"> loading...');
					$.ajax({
						url : "spring/saveRules",
						type : "POST",
						cache : false,
						contentType : "application/x-www-form-urlencoded; charset=UTF-8;",
						data : {
							jsonString : JSON.stringify(copyConditions),
							from : "save",
						},
						// if received a response of any kind from the server
						success : function(responseText) {
							$("#savRuleResponse").html(responseText);
						},
						// if not received any response from the server
						error : function() {
							$("#savRuleResponse").html("No response from server");
						}
					});
		    });
			
			$("#copyRecords").click(function() {
				$("#copyRecResponse").empty(); 
				$("#savRuleResponse").empty();
				
				if(targetTable==="")
				    alert("please select the target table");
				else
				{	
				    getRules();
				    $('#copyRecResponse').html('<img src="css/loading.gif"> loading...');
					$.ajax({
							url : "spring/ajax2Servlet",
							type : "POST",
							cache : false,
							data : {
								jsonString : JSON.stringify(copyConditions),
							},
							// if received a response of any kind from the server
							success : function(responseText) {
								$("#copyRecResponse").html(responseText);
							},
							// if not received any response from the server
							error : function() {
								$("#copyRecResponse").html("no response from server");
							}
 					});
   			    }
         });
});		