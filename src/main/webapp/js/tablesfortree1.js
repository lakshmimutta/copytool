var selColumns=$('<select>').append($('<option>').val('Select Table').text('Select Table'));
var sourceTable;
$(document).ready(function(){
	
		$.ajax({
			url : "spring/getTreeData",
			type : "GET",
			cache : false,
			// if received a response of any kind from the server
			success : function(responseText) {
				var jsonText = responseText;
				$("#tree1").fancytree({
					  extensions: ["filter"],
					  source: [jsonText],
					  activate: function(event, data){
			          },
					  filter: {
					        autoApply: true,
					        // autoExpand: true,
					        mode: "hide"
					      },
				      click:function(event, data){
				    	  $("#copyRecResponse").empty(); 
							$("#savRuleResponse").empty();
						     var node = data.node;
						     var title = data.node.title;
						     sourceTable = title;
						     if(!node.isFolder()){
						    	 $.ajax({
						 			url : "spring/getColumnsServlet",
						 			cache : false,
						 			dataType: "json",
						 			type: "POST",
						 			data : {
						 				tableName : title,
						 			},
						 			// if received a response of any kind from the server
						 			success : function(responseText) {
						 				var sel = $('<select>');
						 				var json = responseText;
						 				$.each(json, function(item,value) {
						 					sel.append($("<option>").attr('value',value).text(value));
						 				      });
						 				
						 				$(".srcTblColumn").empty().append(sel);
						 				$(".trgtTblColumn").empty().append(sel);
						 				$(".itrgtTblColumn").empty().append(sel);
						 				selColumns = sel;
						 			},
						 			// if not received any response from the server
						 			error : function() {
						 				$("#ajaxResponse").html("Could not get columns");
						 			}
						 		});
						     }   
					      }    
					});
				var tree1 = $("#tree1").fancytree("getTree");
				$("input[name=search]").keyup(function(e){
				      var n,        
				        match = $(this).val();

				      if(e && e.which === $.ui.keyCode.ESCAPE || $.trim(match) === ""){
				        $("button#btnResetSearch").click();
				        return;
				      }
				          // Pass a string to perform case insensitive matching
				          n = tree1.filterNodes(match);
				    
				      $("button#btnResetSearch").attr("disabled", false);
				      $("span#matches").text("(" + n + " matches)");
				    }).focus();

				    $("button#btnResetSearch").click(function(e){
				      $("input[name=search]").val("");
				      $("span#matches").text("");
				      tree1.clearFilter();
				    }).attr("disabled", true);
			},
			// if not received any response from the server
			error : function() {
				$("#tree1").html("Could Not load Tables Internal error");
			},
			beforeSend : function() {
				$('.hidden1').html('<img src="css/loading.gif"> loading...');
			},
            complete : function() {
					$('.hidden1').empty();
			},
	});
});
