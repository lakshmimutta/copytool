$(document).ready(function() {
	
	var source_dest = $("#source_dest").val();
	var colName = $("#colName").val();
	

	$("#butnClk").click(function() {
		$.ajax({
			url : "/spring/ajaxServlet",
			type : "POST",
			cache : false,
			contentType : "application/x-www-form-urlencoded; charset=UTF-8;",
			data : {
				source_dest : source_dest,
				colName : colName,
			},
			// if received a response of any kind from the server
			success : function(responseText) {
				$("#ajaxResponse").html(responseText+"source="+source_dest );
			},
			// if not received any response from the server
			error : function() {
				$("#ajaxResponse").html("no response from server");
			}
		});
	});
});