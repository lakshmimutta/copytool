/**
 * 
 */
package com.chrims.products.model;

import java.util.List;

/**
 * @author Annapurna
 *
 */
public class Table {
	
	private String title;
	private String key;
	private boolean folder = false;
	private List<Table> children;

	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getKey() {
		return key;
	}
	public void setKey(String key) {
		this.key = key;
	}
	public List<Table> getChildren() {
		return children;
	}
	public void setChildren(List<Table> children) {
		this.children = children;
	}	
	
	public boolean isFolder() {
		return folder;
	}
	public void setFolder(boolean folder) {
		this.folder = folder;
	}
}
