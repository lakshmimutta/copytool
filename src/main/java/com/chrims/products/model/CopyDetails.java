/**
 * 
 */
package com.chrims.products.model;

import java.util.List;

/**
 * @author Annapurna
 *
 */
   public class CopyDetails {
     String deleteRows;
     List<Rules> rules;
     List<CopyConditions> uprules;
     List<CopyConditions> inrules;
	public CopyDetails()
	{
		
	}
	public CopyDetails(String deleteRows, List<Rules> rules,
			List<CopyConditions> uprules, List<CopyConditions> inrules ) {
		super();
		this.deleteRows = deleteRows;
		this.rules = rules;
		this.uprules = uprules;
		this.inrules = inrules;
	}
	public String getDeleteRows() {
		return deleteRows;
	}
	public void setDeleteRows(String deleteRows) {
		this.deleteRows = deleteRows;
	}
	public List<CopyConditions> getInrules() {
		return inrules;
	}
	public void setInrules(List<CopyConditions> inrules) {
		this.inrules = inrules;
	}
	public List<Rules> getRules() {
		return rules;
	}
	public void setRules(List<Rules> rules) {
		this.rules = rules;
	}
	public List<CopyConditions> getUprules() {
		return uprules;
	}
	public void setUprules(List<CopyConditions> uprules) {
		this.uprules = uprules;
	}
}
