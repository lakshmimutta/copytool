/**
 * 
 */
package com.chrims.products.model;

/**
 * @author Annapurna
 *
 */
public class corporations {
	Integer corp_id;
	String corp_name;
	Integer addr_id;
	public corporations()
	{
		
	}
	public Integer getCorp_id() {
		return corp_id;
	}
	public void setCorp_id(Integer corp_id) {
		this.corp_id = corp_id;
	}
	public String getCorp_name() {
		return corp_name;
	}
	public void setCorp_name(String corp_name) {
		this.corp_name = corp_name;
	}
	public Integer getAddr_id() {
		return addr_id;
	}
	public void setAddr_id(Integer addr_id) {
		this.addr_id = addr_id;
	}
}
