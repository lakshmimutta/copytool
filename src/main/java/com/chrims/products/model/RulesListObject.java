/**
 * 
 */
package com.chrims.products.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * @author Annapurna
 *
 */

@Entity
@Table(name="rules_list")
public class RulesListObject {
	@Id
	@Column(name="rule_name")
	private String ruleName;
	@Column(name="source_table")
	private String sourceTable;
	@Column(name="target_table")
	private String targetTable;
	@Column(name="rules_list")
	private String rulesList;
	@Column(name="delete_rows")
	private String deleteRows;
	@Column(name="update_conditions")
	private String updateRules;
	@Column(name="newrows_updateval")
	private String newUpRules;
	
	public RulesListObject(String ruleName, String sourceTable,
			String targetTable, String rulesList, String deleteRows,
			String updateRules, String newUpRules) {
		super();
		
		this.ruleName = ruleName;
		this.sourceTable = sourceTable;
		this.targetTable = targetTable;
		this.rulesList = rulesList;
		this.deleteRows = deleteRows;
		this.updateRules = updateRules;
		this.newUpRules = newUpRules;
	}
	
	public RulesListObject(){
	}
	public String getNewUpRules() {
		return newUpRules;
	}

	public void setNewUpRules(String newUpRules) {
		this.newUpRules = newUpRules;
	}
	public String getDeleteRows() {
		return deleteRows;
	}
	public void setDeleteRows(String deleteRows) {
		this.deleteRows = deleteRows;
	}
	public String getUpdateRules() {
		return updateRules;
	}
	public void setUpdateRules(String updateRules) {
		this.updateRules = updateRules;
	}
	public String getRuleName() {
		return ruleName;
	}
	public void setRuleName(String ruleName) {
		this.ruleName = ruleName;
	}
	public String getSourceTable() {
		return sourceTable;
	}
	public void setSourceTable(String sourceTable) {
		this.sourceTable = sourceTable;
	}
	public String getTargetTable() {
		return targetTable;
	}
	public void setTargetTable(String targetTable) {
		this.targetTable = targetTable;
	}
	public String getRulesList() {
		return rulesList;
	}
	public void setRulesList(String rulesList) {
		this.rulesList = rulesList;
	}
}
