/**
 * 
 */
package com.chrims.products.model;

/**
 * @author Annapurna
 *
 */
public class CopyConditions {
	private String columnName;
	private String condition;
	private String conditionValue;
	
	public CopyConditions()
	{
		
	}
	public String getColumnName() {
		return columnName;
	}
	public void setColumnName(String columnName) {
		this.columnName = columnName;
	}
	public String getCondition() {
		return condition;
	}
	public void setCondition(String condition) {
		this.condition = condition;
	}
	public String getConditionValue() {
		return conditionValue;
	}
	public void setConditionValue(String conditionValue) {
		this.conditionValue = conditionValue;
	}
	public CopyConditions(String columnName, String condition,
			String conditionValue) {
		this.columnName = columnName;
		this.condition = condition;
		this.conditionValue = conditionValue;
	}
}
