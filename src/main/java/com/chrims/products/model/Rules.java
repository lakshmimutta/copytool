package com.chrims.products.model;

/**
 * @author Annapurna
 *
 */
public class Rules {
	private String columnName;
	private String condition;
	private String conditionValue;
	private String ism;
	private String ise;
	public Rules()
	{
		
	}
	
	public Rules(String columnName, String condition, String conditionValue,
			String ism, String ise) {
		super();
		this.columnName = columnName;
		this.condition = condition;
		this.conditionValue = conditionValue;
		this.ism = ism;
		this.ise = ise;
	}
	public Rules(String columnName, String condition, String conditionValue) {
		super();
		this.columnName = columnName;
		this.condition = condition;
		this.conditionValue = conditionValue;
	}
	public String getColumnName() {
		return columnName;
	}
	public String getIsm() {
		return ism;
	}
	public void setIsm(String ism) {
		this.ism = ism;
	}
	public String getIse() {
		return ise;
	}
	public void setIse(String ise) {
		this.ise = ise;
	}
	public void setColumnName(String columnName) {
		this.columnName = columnName;
	}
	public String getCondition() {
		return condition;
	}
	public void setCondition(String condition) {
		this.condition = condition;
	}
	public String getConditionValue() {
		return conditionValue;
	}
	public void setConditionValue(String conditionValue) {
		this.conditionValue = conditionValue;
	} 
}
