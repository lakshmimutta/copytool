/**
 * 
 */
package com.chrims.products.model;

import java.io.Serializable;
import org.springframework.context.annotation.ScopedProxyMode;
import java.util.List;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

/**
 * @author Annapurna
 *
 */
@Component
@Scope(value="session",proxyMode= ScopedProxyMode.TARGET_CLASS)
public class UserDetails implements Serializable {
	private static final long serialVersionUID = 1L;
	private String name;
	private int userId;
	private List<Integer> roleIds;
	private List<String> roleNames;
	
	
	@Override
	public String toString() {
		return "UserDetails [name=" + name + ", userId=" + userId + ", roleIds=" + roleIds
				+ ", roleNames=" + roleNames + "]";
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getUserId() {
		return userId;
	}
	public void setUserId(int userId) {
		this.userId = userId;
	}
	public List<Integer> getRoleIds() {
		return roleIds;
	}
	public void setRoleIds(List<Integer> roleIds) {
		this.roleIds = roleIds;
	}
	public List<String> getRoleNames() {
		return roleNames;
	}
	public void setRoleNames(List<String> roleNames) {
		this.roleNames = roleNames;
	}
	public UserDetails()
	{
		this.name ="";
		this.userId=0;
		this.roleIds= null;
		this.roleNames = null;
	}
	public void addUserValues(UserDetails user)
	{
		this.name = user.getName();
		this.userId = user.getUserId(); 
		this.roleIds = user.getRoleIds();
		this.roleNames = user.getRoleNames();
	}
	public void clearUserDetails()
	{
		this.name ="";
		this.userId=0;
		this.roleIds= null;
		this.roleNames = null;
	}
}
