/**
 * 
 */
package com.chrims.products.model;

import java.util.List;

/**
 * @author Annapurna
 *
 */
public class RulesObjectMapperForTree {
	private String title;
	private String key;
	private boolean folder = false;
	private String sourceTable="";
	private String TargetTable="";
	private String ruleList="";
	private String deleteRows="";
	private String upRules="";
	private String newRowsUpRules="";
	public String getNewRowsUpRules() {
		return newRowsUpRules;
	}
	public void setNewRowsUpRules(String newRowsUpRules) {
		this.newRowsUpRules = newRowsUpRules;
	}
	public String getDeleteRows() {
		return deleteRows;
	}
	public void setDeleteRows(String deleteRows) {
		this.deleteRows = deleteRows;
	}
	public String getUpRules() {
		return upRules;
	}
	public void setUpRules(String upRules) {
		this.upRules = upRules;
	}
	private List<RulesObjectMapperForTree> children;
	public String getTitle() {
		return title;
	}
	public String getRuleList() {
		return ruleList;
	}
	public void setRuleList(String ruleList) {
		this.ruleList = ruleList;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getKey() {
		return key;
	}
	public void setKey(String key) {
		this.key = key;
	}
	public boolean isFolder() {
		return folder;
	}
	public void setFolder(boolean folder) {
		this.folder = folder;
	}
	public String getSourceTable() {
		return sourceTable;
	}
	public void setSourceTable(String sourceTable) {
		this.sourceTable = sourceTable;
	}
	public String getTargetTable() {
		return TargetTable;
	}
	public void setTargetTable(String targetTable) {
		TargetTable = targetTable;
	}
	public List<RulesObjectMapperForTree> getChildren() {
		return children;
	}
	public void setChildren(List<RulesObjectMapperForTree> children) {
		this.children = children;
	}
}
