/**
 * 
 */
package com.chrims.products.hibernate;


import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;


/**
 * @author Annapurna
 *
 */
public class HibernateUtil {
	protected static final Log logger = LogFactory.getLog(HibernateUtil.class);
	private static SessionFactory sessionFactory;
    
	private static SessionFactory buildSessionFactory() {
        try {
            // Create the SessionFactory from hibernate.cfg.xml
        	/*Configuration configuration = new Configuration().configure();
            ServiceRegistry serviceRegistry
                = new StandardServiceRegistryBuilder()
                    .applySettings(configuration.getProperties()).build();
             
            // builds a session factory from the service registry
            SessionFactory sessionFactory = configuration.buildSessionFactory(serviceRegistry);*/
        	SessionFactory sessionFactory = new Configuration().configure().buildSessionFactory();
            logger.debug("sessionFactory="+sessionFactory);
            return sessionFactory;
        }
        catch (Throwable ex) {
            logger.error("Initial SessionFactory creation failed.", ex);
            throw new ExceptionInInitializerError(ex);
        }
    }
	public static SessionFactory getSessionFactory() {
		if(sessionFactory == null)
			    sessionFactory = buildSessionFactory();
        return sessionFactory;
    }
 }
