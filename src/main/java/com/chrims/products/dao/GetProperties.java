/**
 * 
 */
package com.chrims.products.dao;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * @author Annapurna
 * 
 */
//@Repository
public class GetProperties {
	protected static final Log logger = LogFactory.getLog(GetProperties.class);
	private Properties prop = null;

	public GetProperties(String dbname) {

		InputStream is = null;
		try {
			this.prop = new Properties();
			if (dbname.equals("local"))
				is = this.getClass().getResourceAsStream("/localdb.properties");
			else if (dbname.equals("remote"))
				is = this.getClass().getResourceAsStream("/localdb.properties");
			prop.load(is);
		} catch (FileNotFoundException e) {
			logger.error( "Could not find the properties file!", e);
		} catch (IOException e) {
			logger.error( "Properies file I/O exception !", e);
		} finally {
			try {
				is.close();
			} catch (IOException e) {
				logger.error( "Error closing the properties stream: ", e);
			}
		}
	}
	public String getProperty(String key) {
		return this.prop.getProperty(key);
	}
}
