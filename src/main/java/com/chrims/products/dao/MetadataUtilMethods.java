/**
 * 
 */
package com.chrims.products.dao;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * @author Annapurna
 *
 */
//@Repository
public class MetadataUtilMethods 
{
	static ResultSet resultSet = null;
	static ResultSetMetaData  resultSetMetaData = null;
	static HashSet<String> colNames = null;
	static Statement st=null;
	
	protected static final Log logger = LogFactory.getLog(MetadataUtilMethods.class);
	public static boolean validateColNameInSourceTable(String source,Connection con,String colName)
     {
    	String sql = "select first 1 * from "+source;
        boolean result = false;
        colNames = new HashSet<String>();
    	try {
	    	 st = DatabaseManager.getStatement(con);
	    	 resultSet = DatabaseManager.getResultSet(st, sql);
		     resultSetMetaData = resultSet.getMetaData();
		     
		     int count = resultSetMetaData.getColumnCount();
		     for(int column=1; column<=count; column++)
				{
			        colNames.add(resultSetMetaData.getColumnName(column));
				}
		     if(colNames.contains(colName))
		    		result = true;

		} catch (SQLException e) {
			logger.error( "Failed Validating colName!", e );
		}
    	finally
    	{
    		if(resultSet!=null)
    			DatabaseManager.close(resultSet);
    		if(st!=null)
    			DatabaseManager.close(st);
    	}
    		    	return result;
     }
	 public static List<String> getColumns(String tableName)
	 {
		 List<String> columns=null;
		 Connection con = null;
		 con = DatabaseManager.getLocalConnection();
		 String sql = "select first 1 * from "+tableName;

		 try {
		    	 st = DatabaseManager.getStatement(con);
		    	 resultSet = DatabaseManager.getResultSet(st, sql);
			     resultSetMetaData = resultSet.getMetaData();
			     columns= new ArrayList<String>();
			     
			     int count = resultSetMetaData.getColumnCount();
			     for(int column=1; column<=count; column++)
					{
				        columns.add(resultSetMetaData.getColumnName(column));
					}
			     

			} catch (SQLException e) {
				logger.error( "Exception while loading colNames for the table", e );
			}
	    	finally
	    	{
	    		if(resultSet!=null)
	    			DatabaseManager.close(resultSet);
	    		if(st!=null)
	    			DatabaseManager.close(st);
	    		if(con!=null)
	    			DatabaseManager.close(con);
	    	}
		 return columns;
	 }
}
