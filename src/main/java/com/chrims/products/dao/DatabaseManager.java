/**
 * 
 */
package com.chrims.products.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashSet;
import java.util.Properties;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;


/**
 * @author Annapurna
 * 
 */
//@Repository
public class DatabaseManager {
	public static final Log logger = LogFactory.getLog(DatabaseManager.class);
	public static boolean errorFlag=false;

	public static void loadDriver(String dbDriver) {
		try {
			
			Class.forName(dbDriver);
		} catch (ClassNotFoundException e) {
			logger.error("Load Driver failed!", e);
			errorFlag = true;
		}
	}

	public static Connection getConnection(){
		Connection con = null;
		try {
			
			Properties properties = PropertiesUtil.getProperties();
		    con = DriverManager.getConnection(properties.getProperty("dburl"),properties.getProperty("user"),properties.getProperty("password"));
			
		} catch (SQLException e) {
			logger.error("Get connection failed!", e);
			errorFlag = true;
		}
		return con;
	}
	
	public static Connection getLocalConnection(){
		Connection con = null;
		try {
			
			GetProperties properties = new GetProperties("local");
		    con = DriverManager.getConnection(properties.getProperty("dburl"),properties.getProperty("user"),properties.getProperty("password"));
			
		} catch (SQLException e) {
			logger.error("Get connection failed!", e);
			errorFlag = true;
		}
		return con;
	}
	public static Connection getRemoteConnection(){
		Connection con = null;
		try {
			
			GetProperties properties = new GetProperties("remote");
		    con = DriverManager.getConnection(properties.getProperty("dburl"),properties.getProperty("user"),properties.getProperty("password"));
			
		} catch (SQLException e) {
			logger.error("Get connection failed!", e);
			errorFlag = true;
		}
		return con;
	}
	

	public static PreparedStatement getPreparedStatement(Connection con,
			String sql2) {
		PreparedStatement ps = null;
		try {
			
			ps = con.prepareStatement(sql2);
		} catch (SQLException e) {
			logger.error("get Preapared Statement failed!", e);
			errorFlag = true;
		}
		return ps;
	}

	public static Statement getStatement(Connection con) {
		Statement st = null;
		try {
			st = con.createStatement();
		} catch (SQLException e) {
			logger.error("Get Statment failed!", e);
			errorFlag = true;
		}
		return st;
	}

	public static ResultSet getResultSet(Statement st, String sql) {
		ResultSet rs = null;
		try {
			
			rs = st.executeQuery(sql);
		} catch (SQLException e) {
			logger.error("getResulSet failed!", e);
			errorFlag = true;
		}
		return rs;
	}

	public static void close(PreparedStatement ps) {
		try {
			
			ps.close();
		} catch (SQLException e) {
			logger.error("Closing Prepared Statement failed!", e);
			errorFlag = true;
		}
	}

	public static void close(Statement st) {
		try {
			
			st.close();
		} catch (SQLException e) {
			logger.error("Closing Staetement failed!", e);
			errorFlag = true;
		}
	}

	public static void close(ResultSet rs) {
		try {
			rs.close();
		} catch (SQLException e) {
			logger.error("Closing Result set failed!", e);
			errorFlag = true;
		}
	}

	public static void close(Connection con) {
		try {
			
			if (con != null)
				con.close();
		} catch (SQLException e) {
			logger.error("Closing Connectin failed!", e);
			errorFlag = true;
		}
	}
	public static boolean validate(String colName, HashSet<String> colNames) {
		if (colNames.contains(colName))
			return true;
		else
			return false;
	}
	public static void getPrepStatement()
	{
		
	}
}
