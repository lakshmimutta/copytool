/**
 * 
 */
package com.chrims.products.dao;

import java.sql.Connection;
import java.sql.DatabaseMetaData;

import org.springframework.jdbc.core.JdbcTemplate;

/**
 * @author Annapurna
 *
 */
public interface BunchCopyDataAccess {

	public Connection getConnection(String db);

	public DatabaseMetaData getDatabaseMetaData(String db);
	
	public JdbcTemplate getJdbcTemplate(String db);

}
