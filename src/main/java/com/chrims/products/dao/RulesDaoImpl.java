/**
 * 
 */
package com.chrims.products.dao;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

import com.chrims.products.hibernate.HibernateUtil;
import com.chrims.products.model.RulesListObject;

/**
 * @author Annapurna
 *
 */


public class RulesDaoImpl implements RulesDAO{
     
	protected static final Log logger = LogFactory.getLog(RulesDaoImpl.class);
	
	private SessionFactory sessionFactory;

	public List<RulesListObject> getRulesList() {
		List<RulesListObject> rulesFromDB = new ArrayList<RulesListObject>();
		
		sessionFactory = HibernateUtil.getSessionFactory();

		logger.debug("SessionFactory in dbtables ="+sessionFactory);
	    Session session = sessionFactory.openSession();
	     
	     if(session == null)
	     {
	    	 logger.debug("session not created in dbtables");
	      	 rulesFromDB = null;
	     }
	     else{
	     try{
	        session.beginTransaction();
	        Query query = session.createQuery("from RulesListObject");
	        rulesFromDB = query.list(); 
		    session.getTransaction().commit();
	     }
	     catch(Exception e)
	     {
	    	 logger.error("error getting rules",e);
	    	 rulesFromDB = null;
	     }
	     finally{
	    	 session.close();
	     }
	  }
			return rulesFromDB;
	}

	public String saveRuleHibernateWay(RulesListObject rule)
	{
		sessionFactory = HibernateUtil.getSessionFactory();
		Session session = sessionFactory.openSession();
		String result="something went wrong";
		if(session != null)
		{
			try
			{
			session.beginTransaction();
			session.save(rule);
			session.getTransaction().commit();
			}
			catch(Exception e)
			{
				logger.error("error saving rule to db in hibernate way:", e);
			}
			finally
			{
				session.close();
			}
	        result= "rule save to database successfully";			
		}
		return result;
	}
}
