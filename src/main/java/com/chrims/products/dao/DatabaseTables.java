/**
 * 
 */
package com.chrims.products.dao;

import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;


/**
 * @author Annapurna
 * 
 */
public class DatabaseTables {
	protected static final Log logger = LogFactory.getLog(DatabaseTables.class);

	public static List<String> getTablesList(String db) {
		DatabaseMetaData md;
		ResultSet rs = null;
		Connection con = null;
		if (db.equals("local"))
			con = DatabaseManager.getLocalConnection();
		else if (db.equals("remote"))
			con = DatabaseManager.getRemoteConnection();
		List<String> tablesList = null;
		if (con != null) {
			logger.debug("connection success from database tables" + con);
			try {
				md = con.getMetaData();
				tablesList = new ArrayList<String>();
				String[] types = {"TABLE"};

				rs = md.getTables(null, null, "%", types);
				
				while (rs.next()) {
					tablesList.add(rs.getString(3));
				}
			} catch (SQLException e) {
				System.out.println(e.getStackTrace());
			} finally {
				DatabaseManager.close(rs);
				DatabaseManager.close(con);
			}
		}
		return tablesList;
	}
}
