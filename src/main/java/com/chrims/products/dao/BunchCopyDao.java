/**
 * 
 */
package com.chrims.products.dao;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.chrims.products.model.CopyConditions;
import com.chrims.products.model.RulesListObject;


/**
 * @author Annapurna
 *
 */
public interface BunchCopyDao {
	public List<RulesListObject> getRulesList();
	public String insertRule(RulesListObject rule);
	public Map<String,String> recordsUpdate(String sourceDB,String sTable,String targetDB, String tTable, List<CopyConditions> copyConditions,List<CopyConditions> newRowsUpConditions);
	/**
	 * @param targetDB
	 * @param tTable
	 * @param copyConditions
	 * @param updateValues
	 * @return
	 */
	public Map<String, String> targetRecordsUpdate(String targetDB,
			String tTable, ArrayList<CopyConditions> copyConditions,
			ArrayList<CopyConditions> updateValues);
	public void getUserDetails(String userName, String password);
	public Set<String> getTablesByRoleAccess();
}
