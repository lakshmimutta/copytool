/**
 * 
 */
package com.chrims.products.dao;

import java.util.List;

import com.chrims.products.model.RulesListObject;

/**
 * @author Annapurna
 *
 */

public interface RulesDAO {

	public List<RulesListObject> getRulesList();
	public String saveRuleHibernateWay(RulesListObject rule);

}
