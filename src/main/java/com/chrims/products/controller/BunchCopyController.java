/**
 * 
 */
package com.chrims.products.controller;

import java.io.IOException;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.chrims.products.services.BunchCopyRulesServices;
import com.chrims.products.services.BunchCopyServices;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

/**
 * @author Annapurna
 *
 */
@Controller
public class BunchCopyController {
	
	@Autowired
	private BunchCopyServices copyServices;
	
	@Autowired
	private BunchCopyRulesServices rulesServices;
	
	private static final Logger logger = Logger.getLogger(BunchCopyController.class);
   
	@RequestMapping(value="/getLocalTables", method = RequestMethod.GET)
 	public void getLocalTables(HttpServletResponse response){
    	logger.debug("request reached getLocalTables method");
    	response.setContentType("json");

    	//RulesServices.getTablesList("local");
    	String jsonData = copyServices.getTablesList("local");  
		try {
			response.getWriter().write(jsonData);
		} catch (IOException e) {
		  logger.error("error in getLocalTables controller method",e);	
		}
	}
    
    @RequestMapping(value="/getRemoteTables", method = RequestMethod.GET)
 	public void getRemoteTables(HttpServletResponse response){
    	logger.debug("request reached getRemoteTables method");
    	 response.setContentType("json");
    	 String jsonData = copyServices.getTablesList("remote");
		try {
			response.getWriter().write(jsonData);
		} catch (IOException e) {
		  logger.error("error in getRemoteTables controller method",e);	
		}
	}
    
    @RequestMapping(value="/getSqlServerTables", method = RequestMethod.GET)
 	public void getSqlServerTables(HttpServletResponse response){
    	logger.debug("request reached getSqlServerTables method");
    	 response.setContentType("json");
    	 String jsonData = copyServices.getTablesList("sqlserver");
		try {
			response.getWriter().write(jsonData);
		} catch (IOException e) {
		  logger.error("error in getSqlServerTables controller method",e);	
		}
	}
    
    @RequestMapping(value="/getSqlServerColumns", method = RequestMethod.POST)
 	public void getSqlServerColumns(@RequestParam("tableName") String tableName,HttpServletResponse response){
		logger.debug("Recieved table name at get source columns= " + tableName);
		String result = copyServices.getColumns(tableName,"sqlserver");
		response.setContentType("json");
		try {
			response.getWriter().write(result);
		} catch (IOException e) {
		  logger.error("error in getSqlServerColumns controller method",e);	
		}
 	}
    
    @RequestMapping(value="/getLocalColumns", method = RequestMethod.POST)
 	public void getLocalColumns(@RequestParam("tableName") String tableName,HttpServletResponse response){
		logger.debug("Recieved table name at get source columns= " + tableName);
		String result = copyServices.getColumns(tableName,"local");
		response.setContentType("json");
		try {
			response.getWriter().write(result);
		} catch (IOException e) {
		  logger.error("error in getLocalColumns controller method",e);	
		}
 	}

    @RequestMapping(value="/getRemoteColumns", method = RequestMethod.POST)
 	public void getRemoteColumns(@RequestParam("tableName") String tableName,HttpServletResponse response){
		logger.debug("Recieved table name at get source columns= " + tableName);
		String result = copyServices.getColumns(tableName,"remote");
		response.setContentType("json");
		try {
			response.getWriter().write(result);
		} catch (IOException e) {
		  logger.error("error in getRemoteColumns controller method",e);	
		}
 	}
    
    @RequestMapping(value = "/getRules", method = RequestMethod.GET)
    public void getRules(HttpServletResponse response)
    {
   	 response.setContentType("json");
   	    logger.debug("request reached getAllRules");
   	    
		String jsonData = rulesServices.getRulesList();
		logger.debug("RulesData in BunchCopy Controller: "+jsonData);
		try {
			response.getWriter().write(jsonData);
		} catch (IOException e) {
		  logger.error("error in getRemoteColumns controller method",e);	
		}
    }
    
    @RequestMapping(value="/saveRule", method = RequestMethod.POST)
    public @ResponseBody String saveRule(@RequestParam("jsonString") String jsonString,@RequestParam("from") String from)
    {
    	logger.debug("request reached saveRules method");
		if(from.equals("update"))
			logger.debug("Details from update rules "+from+" :  "+jsonString);
		else
			logger.debug("details from saveRules = " + jsonString);
		String result=	rulesServices.saveRule(jsonString);
		if(result.equals("ok"))
		    return "Rule saved to the database";
		else
			return "Rule with the same name already exists";
    }
    
    @RequestMapping(value = "/copyController", method = RequestMethod.POST)
    public void copyRecords(@RequestParam("jsonString") String jsonString,HttpServletResponse response)
    {
	    logger.debug("details from the client = " + jsonString);
	    response.setContentType("json");
		Map<String, String> result=	copyServices.copyRecordsService(jsonString);
		logger.debug("respone from copy records ="+result);
		Gson gson = new GsonBuilder().setPrettyPrinting()
                .create();
        String json = gson.toJson(result);
        try {
			response.getWriter().write(json);
		} catch (IOException e) {
			e.printStackTrace();
		}
    }
    
    @RequestMapping(value = "/updateController", method = RequestMethod.POST)
    public void updateRecords(@RequestParam("jsonString") String jsonString,HttpServletResponse response){
    	
    	logger.debug("details from the client for update = " + jsonString);
	    response.setContentType("json");
		Map<String, String> result=	copyServices.updateRecordsService(jsonString);
		Gson gson = new GsonBuilder().setPrettyPrinting()
                .create();
		
        String json = gson.toJson(result);
        try {
			response.getWriter().write(json);
		} catch (IOException e) {
			e.printStackTrace();
		}
    }
}
