/**
 * 
 */
package com.chrims.products.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.chrims.products.model.UserDetails;
import com.chrims.products.services.BunchCopyServices;

/**
 * @author Annapurna
 *
 */
@Controller
public class UserSessionController {
	
   protected static final Log logger = LogFactory.getLog(UserSessionController.class);

   @Autowired
   BunchCopyServices bunchCopyServices;
   
   @Autowired
   UserDetails userDetails;
   
   @RequestMapping(value="/login")
   public ModelAndView validateLogin()
   {
	   String view="login",modelMsg = "New User";
	   logger.debug("request reached login.do, userDetails= "+ userDetails);
	   
	   if (userDetails != null && userDetails.getUserId()>0) { 
		   modelMsg = "you are a returning customer";
    	   view = "main";
	   }
       ModelAndView modelAndView = new ModelAndView();
       modelAndView.addObject("message",modelMsg);
       modelAndView.setViewName(view);
       return modelAndView;   
   }
   @RequestMapping(value="/alltables",method=RequestMethod.GET)
   public ModelAndView validateReqToAllTables()
   {
	   String view="alltables",modelMsg = "Registered User";
	   logger.debug("request reached validateAllTables, userDetails= "+ userDetails);
	   
	   if ((userDetails == null) || userDetails.getUserId()== 0){ 
    	   modelMsg = "you need to login ";
    	   view = "login";
	   }	   
       ModelAndView modelAndView = new ModelAndView();
       modelAndView.addObject("message",modelMsg);
       modelAndView.setViewName(view);
       return modelAndView;   
   }
   
   @RequestMapping(value="/main",method=RequestMethod.GET)
   public ModelAndView validateReqToMain()
   {
	   String view="main",modelMsg = "Registered User";
	   logger.debug("request reached validateMainPage, userDetails= "+ userDetails);
	   
	   if (userDetails == null || userDetails.getUserId()==0) { 
		   modelMsg = "Unauthorized user access, need to login first";
    	   view = "login";
	   }
       ModelAndView modelAndView = new ModelAndView();
       modelAndView.addObject("message",modelMsg);
       modelAndView.setViewName(view);
       return modelAndView;   
   }
   
   @RequestMapping(value="/copytables",method=RequestMethod.GET)
   public ModelAndView validateReqToCopyTables()
   {
	   String view="copytables",modelMsg = "Registered User";
	   logger.debug("request reached validate CopyTables, userDetails= "+ userDetails);
	   
	   if (userDetails == null || userDetails.getUserId()==0) { 
		   modelMsg = "you are a returning customer";
    	   view = "login";
	   }
       ModelAndView modelAndView = new ModelAndView();
       modelAndView.addObject("message",modelMsg);
       modelAndView.setViewName(view);
       return modelAndView;   
   }
   
   @RequestMapping(value="/logout")
   public ModelAndView logoutUser()
   {
	   logger.debug("request reached logout.do, userDetails= "+ userDetails);
	   userDetails.clearUserDetails();
	   logger.debug("after clearUserDetails, userDetails= "+ userDetails);
	   ModelAndView modelAndView = new ModelAndView();
       modelAndView.addObject("message","You have successfully logged out");
       modelAndView.setViewName("logout");
       return modelAndView;
   }
   
   @RequestMapping(value="/copyrecordsnew",method=RequestMethod.GET)
   public ModelAndView bunchcopy()
   {
	   String view="copyrecordsnew",modelMsg = "registered User";
	   logger.debug("request reached bunchcopy.do, userDetails "+userDetails);
	   
	   if ((userDetails == null) || userDetails.getUserId()== 0){ 
    	   modelMsg = "you need to login ";
    	   view = "login";   
       }	   
       ModelAndView modelAndView = new ModelAndView();
       modelAndView.addObject("message",modelMsg);
       modelAndView.addObject("userName",userDetails.getName());
       modelAndView.setViewName(view);
       return modelAndView;   
   }
   
 
   @RequestMapping(value="/checkUser",method=RequestMethod.POST)
   public ModelAndView checkUserAddToSession(HttpServletRequest request, HttpServletResponse response)
   {
	   String name,password,view;
	   view = "main";
	   name = request.getParameter("userName");
	   password = request.getParameter("password");

	   
	   String modelMsg="";
	   logger.debug("request reached checkUser, userDetails "+userDetails);

	    bunchCopyServices.getUserDetails(name, password);
	   
		   if(userDetails.getUserId()==0)
		   {
			   modelMsg = "Incorrect UserName or Password";
			   view = "error";   
		   }
		   else if(userDetails.getRoleIds() == null)
		   {	   
			   modelMsg = name+" is a registered unauthorized user as he does not have a role assigned";
		       view = "error";
		   }    
		   else
			   modelMsg = name +" is a registered user";
	    ModelAndView modelAndView = new ModelAndView();
        modelAndView.addObject("message",modelMsg);
        modelAndView.setViewName(view);
        return modelAndView;
   }  
}