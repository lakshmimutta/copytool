package com.chrims.products.controller;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

import org.apache.log4j.Logger;

import com.chrims.products.dao.DatabaseManager;
import com.chrims.products.dao.GetProperties;
import com.chrims.products.hibernate.HibernateUtil;

/**
 * Application Life cycle Listener implementation class ContextListener
 *
 */
public class ContextListener implements ServletContextListener {
	static final Logger logger = Logger.getLogger(ServletContextListener.class);
    public ContextListener() {
   
    }
    public void contextDestroyed(ServletContextEvent arg0) {
    	HibernateUtil.getSessionFactory().close();
    }

	public void contextInitialized(ServletContextEvent SCE) {
	    GetProperties properties = null;
		String driver = "";

		try {
			properties = new GetProperties("local");
			driver = properties.getProperty("drivername");

			DatabaseManager.loadDriver(driver);
		} catch (Exception e) {
			logger.debug("Debug");
			logger.info("Info");
			logger.error("failed!", e);
			e.printStackTrace();
		}
		logger.debug("Loaded driver successfully");
	}

 }

