/**
 * 
 */
package com.chrims.products.controller;

import java.io.IOException;

import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.chrims.products.services.RulesServices;

/**
 * @author Annapurna
 *
 */

@Controller
public class SpringController {
	private static final Logger logger = Logger.getLogger(SpringController.class);

     @RequestMapping(value = "/ajaxServlet", method = RequestMethod.GET)
     public void getRulesForTree(HttpServletResponse response)
     {
    	 response.setContentType("json");
    	logger.debug("request reached doget of Execute rules servlet");
 		String jsonData = RulesServices.getRulesForTree();
 		logger.debug("RulesData in Spring Controller: "+jsonData);
 		try {
			response.getWriter().write(jsonData);
		} catch (IOException e) {
		  logger.error("error in get rules for tree controller method",e);	
		}
     }
    
     @RequestMapping(value = "/ajax2Servlet", method = RequestMethod.POST)
     public @ResponseBody String copyHandler(@RequestParam("jsonString") String jsonString)
     {
 	    logger.debug("details from the client = " + jsonString);
 		String result=	RulesServices.copyRecords(jsonString);
        return result;
     }
     
    @RequestMapping(value="/getColumnsServlet", method = RequestMethod.POST)
 	public void getColumns(@RequestParam("tableName") String tableName,HttpServletResponse response){
		logger.debug("tableName recieved from the tree= " + tableName);
		String result = RulesServices.getColumns(tableName);
		response.setContentType("json");
		try {
			response.getWriter().write(result);
		} catch (IOException e) {
		  logger.error("error in get rules for tree controller method",e);	
		}
 	}

    @RequestMapping(value="/getTreeData", method = RequestMethod.GET)
 	public void getTreeAtSource(HttpServletResponse response){
    	logger.debug("request reached getTreeAtSource method");
    	 response.setContentType("json");
		String jsonData = RulesServices.getTablesInJson("local");
		try {
			response.getWriter().write(jsonData);
		} catch (IOException e) {
		  logger.error("error in get rules for tree controller method",e);	
		}
	}
    
    @RequestMapping(value="/getTree2Data", method = RequestMethod.GET)
 	public void getTreeAtTarget(HttpServletResponse response){
    	logger.debug("request reached getTreeAtTarget method");
		String jsonData = RulesServices.getTablesInJson("remote");
		response.setContentType("json");
		try {
			response.getWriter().write(jsonData);
		} catch (IOException e) {
		  logger.error("error in get rules for tree controller method",e);	
		}
 	}

    @RequestMapping(value="/saveRules", method = RequestMethod.POST)
    public @ResponseBody String saveRulesHandler(@RequestParam("jsonString") String jsonString,@RequestParam("from") String from)
    {
    	logger.debug("request reached saveRules method");
		if(from.equals("update"))
			logger.debug("Details from update rules "+from+" :  "+jsonString);
		else
			logger.debug("details from saveRules = " + jsonString);
		String result=	RulesServices.saveRuleToDB(jsonString,from);
		if(result.equals("ok"))
		    return "Rule saved to the database";
		else
			return "Rule with the same name already exists";
    }
}
