/**
 * 
 */
package com.chrims.products.servicesimpl;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.chrims.products.dao.BunchCopyDao;
import com.chrims.products.model.RulesListObject;
import com.chrims.products.model.RulesObjectMapperForTree;
import com.chrims.products.services.BunchCopyRulesServices;

/**
 * @author Annapurna
 *
 */
@Service
public class BunchCopyRulesServicesImpl implements BunchCopyRulesServices {

	protected static final Log logger = LogFactory.getLog(BunchCopyRulesServicesImpl.class);
	
	@Autowired
	BunchCopyDao copyDao;

	RulesObjectMapperForTree table;
	private ObjectMapper mapper;
	
	public String getRulesList() {
		
		table = new RulesObjectMapperForTree();
		List<RulesListObject> rulesList = copyDao.getRulesList();
		String jsonText=" ";
		if(rulesList == null)
			logger.debug("rules list null");
		else if (rulesList.isEmpty())
			logger.debug("No rules recieved");
		else {
			mapper = new ObjectMapper();
			table.setKey("0");
			table.setTitle("RulesList");
			table.setFolder(true);
			List<RulesObjectMapperForTree> list = new ArrayList<RulesObjectMapperForTree>();
			RulesObjectMapperForTree tab;
			int i = 0;
			for (RulesListObject rule : rulesList) {
				tab = new RulesObjectMapperForTree();
				tab.setKey((i++) + "");
				tab.setRuleList(rule.getRulesList());
				tab.setTitle(rule.getRuleName());
				tab.setSourceTable(rule.getSourceTable());
				tab.setTargetTable(rule.getTargetTable());
				tab.setDeleteRows(rule.getDeleteRows());
				tab.setUpRules(rule.getUpdateRules());
				tab.setNewRowsUpRules(rule.getNewUpRules());
				list.add(tab);
			}
			table.setChildren(list);
			boolean errorFlag;
			try {
			
				jsonText = mapper.writeValueAsString(table);
			} catch (JsonGenerationException e) {
				logger.error("error in object mapping to json"+e);
				errorFlag = true;
			} catch (JsonMappingException e) {
				logger.error("error in object mapping to json"+e);
				errorFlag = true;
			} catch (IOException e) {
				logger.error("error in object mapping to json"+e);
				errorFlag = true;
			}
		}
		return jsonText;
	}

	public String saveRule(String rule) {
		
		return null;
	}

	public String updateRule(String rule) {
		return null;
	}
}
