/**
 * 
 */
package com.chrims.products.servicesimpl;

import java.io.IOException;
import java.sql.DatabaseMetaData;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.log4j.Logger;
import org.codehaus.jackson.JsonNode;
import org.codehaus.jackson.JsonProcessingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.type.TypeReference;
import org.json.simple.JSONArray;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.chrims.products.dao.BunchCopyDao;
import com.chrims.products.dao.BunchCopyDataAccess;
import com.chrims.products.model.CopyConditions;
import com.chrims.products.model.UserDetails;
import com.chrims.products.services.BunchCopyServices;

/**
 * @author Annapurna
 *
 */
@Service
public class BunchCopyServicesImpl implements BunchCopyServices{

	private static final Logger logger = Logger.getLogger(BunchCopyServicesImpl.class);
	
	@Autowired
	private BunchCopyDataAccess dataAccess;
	@Autowired
	private BunchCopyDao bunchCopyDao;
	
	@Autowired
	private UserDetails user;
	
	private ObjectMapper mapper;

	public String getTablesList(String db) {
		
		logger.debug("request reached BunchCopyServicesImpl.getTablesList, user with role= "+user);
		List<String> tables = null;
		String result="Problem getting metadata in getTablesList";

		if(dataAccess!=null){
			DatabaseMetaData metadata = dataAccess.getDatabaseMetaData(db);
			ResultSet resultSet;
			tables = new ArrayList<String>();
			try {
				String[] types = {"TABLE"};
				
				resultSet = metadata.getTables(null, null, "%", types);
				while (resultSet.next()) {
					tables.add(resultSet.getString("TABLE_NAME"));
				}
			}	
			catch (SQLException e) {
				e.printStackTrace();
			}				
	    }
		Set<String> restrictedTablesSet = bunchCopyDao.getTablesByRoleAccess();
		logger.debug("restricted tables list: "+restrictedTablesSet);
	 if(!tables.isEmpty())
	 {
		if (restrictedTablesSet != null && restrictedTablesSet.size()>0) 
		{
			List<String> restrictedTablesList = new ArrayList<String>(restrictedTablesSet);
			for(String table: restrictedTablesList)
			{	
                  if (tables.contains(table))
                	  tables.remove(table);
		    }	
		}	
			JSONArray json = new JSONArray();
			json.addAll(tables);
			result = json.toJSONString();
	  }	
	    return result;
	}

	@SuppressWarnings("unchecked")
	public String getColumns(String tableName, String db) {

		String result=null;
		if(dataAccess!=null){
			logger.debug("request reached BunchCopyServicesImpl.getColumns");
			DatabaseMetaData metadata = dataAccess.getDatabaseMetaData(db);
			ResultSet resultSet=null;
			List<String> columns = new ArrayList<String>();
			try {
				resultSet = metadata.getColumns(null, null, tableName, null );
				while(resultSet.next()) {
					
				    String columnName = resultSet.getString(4);
				    columns.add(columnName);
				}
				JSONArray json = new JSONArray();
				json.addAll(columns);
				result = json.toJSONString();
			} catch (SQLException e) {
				e.printStackTrace();
			}				
		}
		return result;
	}

	public Map<String, String> copyRecordsService(String jsonString) {
	   Map<String, String> result= new HashMap<String,String>();
	   String sTable,tTable,targetDB,sourceDB;

	   logger.debug("Request reached copyRecordsService"+jsonString);
	   JsonNode configNode,sourcedbNode,targetdbNode,tTableNode,sTableNode,rootNode,defValuesNode;
	   ArrayList<CopyConditions> copyConditions = new ArrayList<CopyConditions>();
	   ArrayList<CopyConditions> newRowsUpConditions = new ArrayList<CopyConditions>();
	   mapper = new ObjectMapper();
	   try {

		   mapper.readTree(jsonString);
		   rootNode = mapper.readTree(jsonString);
		   configNode = rootNode.get("config");
		   TypeReference<List<CopyConditions>> typeRef = new TypeReference<List<CopyConditions>>(){};
		   
		   copyConditions = mapper.readValue(configNode.traverse(), typeRef);
		   logger.debug(copyConditions);
		   
		   sourcedbNode = rootNode.get("sourceDB");
		   sourceDB = sourcedbNode.getTextValue();
		   	   
		   targetdbNode = rootNode.get("targetDB");
		   targetDB = targetdbNode.getTextValue();
		   
		   defValuesNode = rootNode.get("newRowsUpdtCond");
		   newRowsUpConditions = mapper.readValue(defValuesNode.traverse(), typeRef);
		   
		   tTableNode = rootNode.get("target");
		   tTable = tTableNode.getTextValue();
		   
		   sTableNode = rootNode.get("source");
		   sTable = sTableNode.getTextValue();
		   
		   result = bunchCopyDao.recordsUpdate(sourceDB, sTable, targetDB, tTable, copyConditions,newRowsUpConditions);
	       logger.debug("results from recordsUpdateDao="+result);
	   } catch (JsonProcessingException e) {
         logger.debug("Exception processing json data");
         logger.error(e);
	   } catch (IOException e) {
		 logger.error(e);
	   } 
	   return result;
	}

	/* (non-Javadoc)
	 * @see com.chrims.products.services.BunchCopyServices#updateRecordsService(java.lang.String)
	 */
	public Map<String, String> updateRecordsService(String jsonString) {
		   Map<String, String> result= new HashMap<String,String>();
		   String tTable,targetDB;

		   logger.debug("Request reached copyRecordsService"+jsonString);
		   JsonNode configNode,targetdbNode,tTableNode,rootNode,updateValNode;
		   ArrayList<CopyConditions> copyConditions = new ArrayList<CopyConditions>();
		   ArrayList<CopyConditions> updateValues = new ArrayList<CopyConditions>();
		   mapper = new ObjectMapper();
		   try {

		   mapper.readTree(jsonString);
		   TypeReference<List<CopyConditions>> typeRef = new TypeReference<List<CopyConditions>>(){};
		   
		   rootNode = mapper.readTree(jsonString);
		   
		   configNode = rootNode.get("config");
		   copyConditions = mapper.readValue(configNode.traverse(), typeRef);
		   logger.debug(copyConditions);
		   
		   updateValNode = rootNode.get("outdtCond");
		   updateValues = mapper.readValue(updateValNode.traverse(), typeRef);
		   logger.debug(updateValues);

		   targetdbNode = rootNode.get("targetDB");
		   targetDB = targetdbNode.getTextValue();
		   
		   tTableNode = rootNode.get("target");
		   tTable = tTableNode.getTextValue();

		   result = bunchCopyDao.targetRecordsUpdate(targetDB, tTable, copyConditions, updateValues);
	
	   }catch (JsonProcessingException e) {
      logger.debug("Exception processing json data");
      logger.error(e);
	   } catch (IOException e) {
		 logger.error(e);
	   }
	   return result;
	}
	
	public void getUserDetails(String userName, String password) {

		logger.debug("request reached getUserDetails service method");
        	bunchCopyDao.getUserDetails(userName, password);
	}
}
