/**
 * 
 */
package com.chrims.products.services;


/**
 * @author Annapurna
 *
 */
public interface BunchCopyRulesServices {

	public String getRulesList();
	public String saveRule(String rule);
	public String updateRule(String rule);
}
