/**
 * 
 */
package com.chrims.products.services;

import java.io.IOException;
import java.sql.Connection;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.JsonNode;
import org.codehaus.jackson.JsonProcessingException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.type.TypeReference;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import com.chrims.products.dao.DatabaseManager;
import com.chrims.products.dao.DatabaseTables;
import com.chrims.products.dao.MetadataUtilMethods;
import com.chrims.products.dao.RulesDAO;
import com.chrims.products.dao.RulesDaoImpl;
import com.chrims.products.model.CopyDetails;
import com.chrims.products.model.Rules;
import com.chrims.products.model.RulesListObject;
import com.chrims.products.model.RulesObjectMapperForTree;
import com.chrims.products.model.Table;
import com.chrims.products.model.CopyConditions;
import com.chrims.products.util.TablesCopyThread;

/**
 * @author Annapurna
 */

public class RulesServices {
	public static Map<String, String> tablesMap;
	public static ObjectMapper mapper;
	public static boolean errorFlag = false;
	
	static RulesDAO rulesDaoImpl = new RulesDaoImpl();
	protected static final Log logger = LogFactory.getLog(RulesServices.class);

	public static Map<String, String> getTableNames(String jsonString) {
		JSONParser parser = new JSONParser();
		String source="";
		String target="";
		tablesMap = new HashMap<String, String>();
		try {
			
			Object obj = parser.parse(jsonString);
			JSONObject jsonObject = (JSONObject) obj;
			//JSONArray rulesList = (JSONArray) jsonObject.get("config");
            source = (String) jsonObject.get("source");
            target = (String) jsonObject.get("target");
			
            tablesMap.put(source, target);
			for (String sourceTable : tablesMap.keySet()) {
				logger.debug(sourceTable + ":" + tablesMap.get(sourceTable));
			}
		} catch (Exception e) {
			logger.debug("jsonString= " + jsonString + " " + e.getStackTrace());
			errorFlag = true;
		}
		return tablesMap;
	}

	public static CopyDetails getRules(String jsonString) 
	{
		mapper = new ObjectMapper();				
		
        JsonNode rootnode,configNode,upNode,delNode,inNode;
        List<Rules> rulesList=null;
        List<CopyConditions> uprules=null;
        List<CopyConditions> inrules=null;
        String deleteRows="";
        CopyDetails copyDetails=null;
		try {
			
			rootnode = mapper.readTree(jsonString);
			configNode = rootnode.get("config");
			TypeReference<List<Rules>> typeRef = new TypeReference<List<Rules>>(){};
			rulesList = mapper.readValue(configNode.traverse(), typeRef);
			logger.debug(rulesList);

			upNode = rootnode.get("outdtCond");
			TypeReference<List<CopyConditions>> typeRef2 = new TypeReference<List<CopyConditions>>(){};
			uprules = mapper.readValue(upNode.traverse(), typeRef2);
			logger.debug("uprules="+uprules);
			
			inNode = rootnode.get("newRowsUpdtCond");
			inrules = mapper.readValue(inNode.traverse(), typeRef2);
			logger.debug("inrules="+inrules);
			
			delNode = rootnode.path("delTrgtRows");
			deleteRows = delNode.getTextValue();
			logger.debug("deleteRows"+deleteRows);
			
			copyDetails = new CopyDetails(deleteRows, rulesList, uprules, inrules);
		} 
		  catch (JsonProcessingException e) {
            logger.error("rule to database error", e);
            errorFlag = true;
		} catch (IOException e) {
			logger.error("rule to database error", e);
			errorFlag = true;
		}
		return copyDetails;
	}
	
	public static String saveRuleToDB(String jsonString, String from)
	{
		JSONParser parser = new JSONParser();				
		String result="ok";
        String updateRules="",newRowsUpdateVal="";
        try
        {
				Object obj = parser.parse(jsonString);
				JSONObject jsonObject = (JSONObject) obj;
				String ruleName = (String) jsonObject.get("ruleName");
				String source = (String) jsonObject.get("source");
				String target = (String) jsonObject.get("target");
				JSONArray ruleList = (JSONArray) jsonObject.get("config");
				String rulesList = ruleList.toString();
				String deleteRows = (String) jsonObject.get("delTrgtRows");
				JSONArray updateRList = (JSONArray) jsonObject.get("outdtCond");
				JSONArray updateNList = (JSONArray) jsonObject.get("newRowsUpdtCond");          
				updateRules = updateRList.toString();
				newRowsUpdateVal = updateNList.toString();
				
				logger.debug("parsed="+ruleName+"  "+source+"  "+target+"  "+rulesList+" "+deleteRows+" "+updateRules + " " + newRowsUpdateVal);
				
				RulesListObject rule = new RulesListObject(ruleName, source,
						target, rulesList, deleteRows, updateRules, newRowsUpdateVal);
				rulesDaoImpl.saveRuleHibernateWay(rule);
        }
        catch(Exception e)
        {
        	logger.error("save rule exception",e);
        }
		return result;
	}
	
	public static String getTablesInJson(String db) {
		List<String> tables = DatabaseTables.getTablesList(db);
		String jsonText=" ";
		if (tables == null)
			logger.debug("Empty tables");
		else if (tables.isEmpty())
			logger.debug("No tables recieved");
		else {
			mapper = new ObjectMapper();
			Table table = new Table();
			table.setKey("0");
			table.setTitle("Tables");
			table.setFolder(true);
			List<Table> list = new ArrayList<Table>();
			Table tab;
			int i = 0;
			for (String tableName : tables) {
				tab = new Table();
				tab.setKey((i++) + "");
				tab.setTitle(tableName);
				list.add(tab);
			}
			table.setChildren(list);
			try {
			
				jsonText = mapper.writeValueAsString(table);
			} catch (JsonGenerationException e) {
				logger.error("error in object mapping to json"+e);
				errorFlag = true;
			} catch (JsonMappingException e) {
				logger.error("error in object mapping to json"+e);
				errorFlag = true;
			} catch (IOException e) {
				logger.error("error in object mapping to json"+e);
				errorFlag = true;
			}
		}
		return jsonText;
	}
	
	public static String getRulesForTree()
	{
		
		List<RulesListObject> rulesList = rulesDaoImpl.getRulesList();
		String jsonText=" ";
		if(rulesList == null)
			logger.debug("rules list null");
		else if (rulesList.isEmpty())
			logger.debug("No rules recieved");
		else {
			mapper = new ObjectMapper();
			RulesObjectMapperForTree table = new RulesObjectMapperForTree();
			table.setKey("0");
			table.setTitle("RulesList");
			table.setFolder(true);
			List<RulesObjectMapperForTree> list = new ArrayList<RulesObjectMapperForTree>();
			RulesObjectMapperForTree tab;
			int i = 0;
			for (RulesListObject rule : rulesList) {
				tab = new RulesObjectMapperForTree();
				tab.setKey((i++) + "");
				tab.setRuleList(rule.getRulesList());
				tab.setTitle(rule.getRuleName());
				tab.setSourceTable(rule.getSourceTable());
				tab.setTargetTable(rule.getTargetTable());
				tab.setDeleteRows(rule.getDeleteRows());
				tab.setUpRules(rule.getUpdateRules());
				tab.setNewRowsUpRules(rule.getNewUpRules());
				list.add(tab);
			}
			table.setChildren(list);
			try {
			
				jsonText = mapper.writeValueAsString(table);
			} catch (JsonGenerationException e) {
				logger.error("error in object mapping to json"+e);
				errorFlag = true;
			} catch (JsonMappingException e) {
				logger.error("error in object mapping to json"+e);
				errorFlag = true;
			} catch (IOException e) {
				logger.error("error in object mapping to json"+e);
				errorFlag = true;
			}
		}
		return jsonText;
	}
	
	public static String copyRecords(String jsonString){ 
		
		CopyDetails copyDetails=null;
		Connection sourceCon = null;
		Connection targetCon = null;
		Map<String, String> tablesMap = null;
        String result = "successfully copied records";
		
		try {
			sourceCon = DatabaseManager.getLocalConnection();
			targetCon = DatabaseManager.getRemoteConnection();
			if (sourceCon != null && targetCon != null) {
				logger.debug("source Connection success " + sourceCon);
				logger.debug("target Connection success " + targetCon);
				tablesMap = new HashMap<String, String>(
						RulesServices.getTableNames(jsonString));
				copyDetails = RulesServices.getRules(jsonString);
				ExecutorService executor = Executors.newFixedThreadPool(3);
				
					for (String sourceTable : tablesMap.keySet()) {
						Runnable worker = new TablesCopyThread(sourceCon,
								sourceTable, tablesMap.get(sourceTable),targetCon,copyDetails);
						executor.execute(worker);
					}
					executor.shutdown();
					while (!executor.isTerminated()) {
					}
					if(TablesCopyThread.errorFalg || DatabaseManager.errorFlag || RulesServices.errorFlag)
					{	
						TablesCopyThread.errorFalg = false;
						DatabaseManager.errorFlag = false;
						RulesServices.errorFlag = false;
						result="Could not copy records";
					}	
					else{
					logger.debug("Finished all threads");
				}
			}					
		
		} catch (Exception ex) {
			logger.error("failed!", ex);
			result="Could not copy records";
		} finally {
			DatabaseManager.close(sourceCon);
			DatabaseManager.close(targetCon);
		}
		return result;
	}
	@SuppressWarnings("unchecked")
	public static String getColumns(String tableName){
		String result = "";
		List<String> colNames = MetadataUtilMethods.getColumns(tableName);
		
		logger.debug("column names from the utilMethod");
		for(String col: colNames)
            logger.debug("col "+col);
		if (!colNames.isEmpty()) {
			JSONArray json = new JSONArray();
			json.addAll(colNames);
			result = json.toJSONString();
		}
		return result;
	}
    @SuppressWarnings("unchecked")
	public static String getTablesList(String db)
    {
       List<String> tables = DatabaseTables.getTablesList(db);
       String result = "";
       if (!tables.isEmpty()) {
			JSONArray json = new JSONArray();
			json.addAll(tables);
			result = json.toJSONString();
	    }
       else
    	   result = "No Tables";
       return result;
    }
}

