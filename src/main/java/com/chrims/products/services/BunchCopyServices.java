/**
 * 
 */
package com.chrims.products.services;

import java.util.Map;

/**
 * @author Annapurna
 *
 */
public interface BunchCopyServices {

	public String getTablesList(String db);
	public String getColumns(String tableName,String db);
	public Map<String, String> copyRecordsService(String json);
	public Map<String, String> updateRecordsService(String jsonString);
	public void getUserDetails(String userName, String password);
}
