/**
 * 
 */
package com.chrims.products.util;

import java.sql.Connection;
import java.sql.Statement;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Callable;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.chrims.products.dao.DatabaseManager;
import com.chrims.products.model.CopyConditions;

/**
 * @author Annapurna
 *
 */
public class UpdateRecordsThread implements Callable <Map<String,String>> {

	  private Connection targetCon;
	  private String targetTable;
	  private List<CopyConditions> copyConditions;
      private List<CopyConditions> updateValues;	  
	   
      protected static final Log logger = LogFactory.getLog(UpdateRecordsThread.class);
      
      public UpdateRecordsThread(Connection targetCon,String targetTable, List<CopyConditions> copyConditions,List<CopyConditions> updateValues)
      {
    	  this.targetCon = targetCon;
    	  this.targetTable = targetTable;
    	  this.copyConditions = copyConditions;
    	  this.updateValues = updateValues;
      }
      
	 	public Map<String, String> call() throws Exception {

	 	Map<String,String> resultMap = new HashMap<String,String>();
	 	resultMap.put("message","ok");
	 	resultMap.put("rowsUpdated", Integer.toString(0));
		int rowsCount=0;
		Statement updateSt = null;
	    String whereCondition="", upSql="";
		try {
			if (copyConditions != null && copyConditions.size() > 0) {
				whereCondition = UtilityMethods
						.buildWhereFromConditions(copyConditions);
			}
			logger.debug("where Condition =" + whereCondition);

			if (updateValues != null && updateValues.size() > 0) {
				upSql = "update " + targetTable + " set";
				if (copyConditions.size() > 0)
					upSql = upSql + " "
							+ UtilityMethods.getSetClause(updateValues)
							+ whereCondition;
				else
					upSql = upSql +" "
							+ UtilityMethods.getSetClause(updateValues);
				logger.debug("upSql statement =" + upSql);
				updateSt = DatabaseManager.getStatement(targetCon);
				rowsCount = updateSt.executeUpdate(upSql);
                resultMap.put("rowsUpdated", Integer.toString(rowsCount));
			}
		} catch (Exception e) {
          logger.error(e);
          resultMap.put("message",e.getMessage());
		} finally {
			DatabaseManager.close(targetCon);
			DatabaseManager.close(updateSt);
		}
		return resultMap;
	}
}
