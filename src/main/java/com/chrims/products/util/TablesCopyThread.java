/**
 * 
 */
package com.chrims.products.util;

import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.HashSet;
import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.chrims.products.dao.DatabaseManager;
import com.chrims.products.model.CopyDetails;
import com.chrims.products.model.Rules;
import com.chrims.products.model.CopyConditions;

/**
 * @author Annapurna
 *
 */
public class TablesCopyThread implements Runnable
{
  private Connection sourceCon;
  private Connection targetCon;
  private String sourceTable;
  private String targetTable;
  private CopyDetails copyDetails;
  
   
  protected static final Log logger = LogFactory.getLog(TablesCopyThread.class);
  public static boolean errorFalg = false;
  
  public TablesCopyThread(Connection sourceCon,String source, String target,Connection targetCon,CopyDetails copyDetails ) 
  {
	this.sourceCon = sourceCon;
	this.targetCon = targetCon;
	this.sourceTable = source;
	this.targetTable = target;
	this.copyDetails = copyDetails;
  }
  
  public void run() 
  {
	  Statement st=null,delSt=null;
		
		String columns = "";
		String valueParams = "";
		String sql2 ="";
		String deleteSql="";
		
		ResultSet resultSet = null;
	    ResultSetMetaData  resultSetMetaData = null;
	    PreparedStatement ps=null, psDelete=null;
	    HashSet<String> colNames = null;
	    String whereCondition="", upSql="";
	    List<Rules> rules = copyDetails.getRules();
	    String deleteRows = copyDetails.getDeleteRows();
	    List<CopyConditions> upRules = copyDetails.getUprules();
	    List<CopyConditions> inRules = copyDetails.getInrules();
	    int rowCount=0;
	    String sql = "select * from "+ targetTable +" where 1 = 2";
	    logger.debug("sql=   "+sql);
    	st = DatabaseManager.getStatement(targetCon);
    	ResultSet resultSetcols = DatabaseManager.getResultSet(st, sql);
    	
    	try {	
    	resultSetMetaData = resultSetcols.getMetaData();
    	colNames = new HashSet<String>();
    	int count = resultSetMetaData.getColumnCount();
        
    	for(int column=1; column<=count; column++)
		{
		       columns += resultSetMetaData.getColumnName(column);
		       colNames.add(resultSetMetaData.getColumnName(column));
		       valueParams += "?";
		       if(column!=count)
		       {
		    	   columns += ",";
		    	   valueParams += ",";
		       }
			}
	    
	    sql = "select "+columns+" from "+ sourceTable;
	    if(rules.size()>0)
	    {
	    	whereCondition = UtilityMethods.buildWhereClause(rules);
           sql = sql + whereCondition;
	    }   
        logger.debug("sql statement ="+sql);
     	
       	 targetCon.setAutoCommit(false);

       	 if(deleteRows.equals("false") && ( upRules != null && upRules.size()>0))
       	 {
       		 upSql = "update "+targetTable+" set ";
	        	if(rules.size()>0)
	        	   upSql = upSql+" "+UtilityMethods.getSetClause(upRules)+whereCondition;
	        	else
	        	   upSql = upSql+" "+UtilityMethods.getSetClause(upRules);	
	        	logger.debug("upSql statement ="+upSql);
	        	delSt = DatabaseManager.getStatement(targetCon);
	        	delSt.execute(upSql);
       	 }
         else if(deleteRows.equals("true"))
	      {
    	    	deleteSql = "delete from "+targetTable;
    	    	if(rules.size()>0)
	        	deleteSql = deleteSql + whereCondition;
	        	psDelete = DatabaseManager.getPreparedStatement(targetCon, deleteSql);
	        	logger.debug("deleteSql statement ="+deleteSql);
	        	int deletedRows = psDelete.executeUpdate();
	        	logger.debug("Deleted rows from target= "+deletedRows);
	      }
	        
	    	st = DatabaseManager.getStatement(sourceCon);
	    	resultSet = DatabaseManager.getResultSet(st, sql);
	    	
	    	sql2 = "insert into "+targetTable+"("+columns+") values ("+valueParams+")";
	    	ps = DatabaseManager.getPreparedStatement(targetCon, sql2);
	    	logger.debug("preparedstament="+sql2);
	    	
	    	int type=0;
	    	String colName="",colVal="";
	    	Map<String,String> inRulesMap=null;
	    	if( inRules != null && inRules.size() > 0)
	    		inRulesMap = UtilityMethods.getMap(inRules);
	    	SimpleDateFormat formatter = new SimpleDateFormat("yyyy/MM/dd");
	    	while(resultSet.next())
	    	{
	    		for(int column=1; column<=count; column++)
	    		{
	    	
	                /*logger.debug("name: "+resultSetMetaData.getColumnName(column));
	                logger.debug("Java type: " +resultSetMetaData.getColumnType(column));
	                logger.debug("Java class name: "+resultSetMetaData.getColumnClassName(column));
	                logger.debug("column type name: "+resultSetMetaData.getColumnTypeName(column));*/
	                colName = resultSetMetaData.getColumnName(column);
	    			type = resultSetMetaData.getColumnType(column);
	    			colVal="";
	    			if((inRules!=null && inRules.size() > 0) && (inRulesMap!=null && inRulesMap.size() > 0 && inRulesMap.keySet().contains(colName)))
	    			{
	    				colVal = inRulesMap.get(colName);
	    			}
	    			
	    			if(type == -7 || type == -6)
	    		    {
                        if(colVal=="")
                        	ps.setShort(column, resultSet.getShort(column));
                        else
                        {
                        	ps.setShort(column, resultSet.getShort(Short.valueOf(colVal)));
                        }
	    		    } 
	    			else if(type == 4 || type == 5 || type == 2)
	    		    {
                        if(colVal=="")
                        	ps.setInt(column, resultSet.getInt(column));
                        else
                        {
                        	ps.setInt(column, resultSet.getShort(Integer.valueOf(colVal)));
                        }
	    		    }
	    		    else if(type == 12)
	    		    {
                        if(colVal=="")
                        	ps.setString(column, resultSet.getString(column));
                        else 
                        	ps.setString(column, resultSet.getString(colVal));
	    		    }
	    		    else if(type == 1 || type == 12 || type == -1 )
	    		    {
	    		    	ps.setCharacterStream(column, resultSet.getCharacterStream(column));
	    		    }
	    		    else if(type == 3)
	    		    {
	    		    	if(colVal == "")
	    		    		ps.setBigDecimal(column, resultSet.getBigDecimal(column));
	    		    	else
	    		    	{
	    		    		BigDecimal bdecimal = new BigDecimal(colVal);
	    		    		ps.setBigDecimal(column,bdecimal);	    		    		
	    		    	}	
	    		    }
	    		    else if(type == 8)
	    		    {
	    		    	ps.setDouble(column, resultSet.getDouble(column));
	    		    }
	    		    else if(type == 6 || type == 7)
	    		    {
	    		    	ps.setFloat(column, resultSet.getFloat(column));
	    		    }
	    		    else if(type == 91)
	    		    {
	    		    	if(colVal=="")
	    		    	    ps.setDate(column, resultSet.getDate(column));
	    		    	else
		    			{
		    			     colVal = inRulesMap.get(colName);
		    			     java.util.Date parsed = formatter.parse(colVal);
		    			     java.sql.Date sqlDate = new java.sql.Date(parsed.getTime());
		    			     logger.debug("datevalueof = "+parsed);
		    			     
		    			     ps.setDate(column, sqlDate);
		    			}
	    		    }
	    		    else if(type == 92)
	    		    {
	    		    	ps.setTime(column, resultSet.getTime(column));
	    		    }
	    		    else if(type == 2003)
	    		    {
	    		    	ps.setArray(column, resultSet.getArray(column));
	    		    }
	    		    else if(type == 2004)
	    		    {
	    		    	ps.setBlob(column, resultSet.getBlob(column));
	    		    }
	    		    else if(type == 2005)
	    		    {
	    		    	ps.setClob(column, resultSet.getClob(column));
	    		    }
	    		}
	    		ps.addBatch();rowCount++;
	    	}
	    	logger.debug("rows executed from sourcetable= "+rowCount);
	    	int[] rc = ps.executeBatch();
		    logger.debug( targetTable +" rows inserted: "+ rc.length);
		    
		    logger.debug("Thread: "+Thread.currentThread().getName()+"is running");
		    targetCon.commit();
            }
	        catch (Exception e) {
	        	
	        	logger.error( "Threads failed!", e );
	        	errorFalg = true;
	        	try {
					targetCon.rollback();
				} catch (SQLException e1) {
					errorFalg = true;
					e1.printStackTrace();
				}		
		}
	    finally
	    {
    	    	if(resultSet!=null)
	         		DatabaseManager.close(resultSet);
	    		if(st!=null)
	    			DatabaseManager.close(st);
	    		if(delSt!=null)
	    			DatabaseManager.close(st);
				if(ps!=null)
	    		   DatabaseManager.close(ps);
	    }
    }
}
