/**
 * 
 */
package com.chrims.products.util;

import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Callable;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.chrims.products.dao.DatabaseManager;
import com.chrims.products.model.CopyConditions;

/**
 * @author Annapurna L Mutta
 *
 */

public class CopyRecordsThread implements Callable <Map<String,String>>{
	  private Connection sourceCon;
	  private Connection targetCon;
	  private String sourceTable;
	  private String targetTable;
	  private List<CopyConditions> copyConditions;
	  private List<CopyConditions> newRowsUpdateValues;
	  
	  protected static final Log logger = LogFactory.getLog(CopyRecordsThread.class);
	  public static boolean errorFalg = false;
	  
	  public CopyRecordsThread(Connection sourceCon,String source, Connection targetCon,String target,List<CopyConditions> copyConditions,List<CopyConditions> newRowsUpdateVal) 
	  {
		this.sourceCon = sourceCon;
		this.targetCon = targetCon;
		this.sourceTable = source;
		this.targetTable = target;
		this.newRowsUpdateValues = newRowsUpdateVal;
		this.copyConditions = copyConditions;
	  }
	  
	  public Map<String, String> call() throws Exception 
	  {
		  Statement st=null,delSt=null;
		    Map<String,String> resultMap = new HashMap<String,String>();
		    int rowsCount=0;
            			
			String columns = "";
			String valueParams = "";
			String sql2 ="";
			
			ResultSet resultSet = null;
		    ResultSetMetaData  resultSetMetaData = null;
		    PreparedStatement ps=null;
		    HashSet<String> colNames = null;
		    String whereCondition="";
		    int rowCount=0;
                        		    
		    String sql = "select * from "+ targetTable +" where 1 = 2";
		    logger.debug("sql=  "+sql);
	    	st = DatabaseManager.getStatement(targetCon);
	    	ResultSet resultSetcols = DatabaseManager.getResultSet(st, sql);
	    	
	    	try {	
	    	resultSetMetaData = resultSetcols.getMetaData();
	    	colNames = new HashSet<String>();
	    	int count = resultSetMetaData.getColumnCount();
	        
	    	for(int column=1; column<=count; column++)
			{
			       columns += resultSetMetaData.getColumnName(column);
			       colNames.add(resultSetMetaData.getColumnName(column));
			       valueParams += "?";
			       if(column!=count)
			       {
			    	   columns += ",";
			    	   valueParams += ",";
			       }
			 }
		    
		    sql = "select "+columns+" from "+ sourceTable;
		    if(copyConditions.size()>0)
		    {
		    	whereCondition = UtilityMethods.buildWhereFromConditions(copyConditions);
	           sql = sql + whereCondition;
		    }   
	        logger.debug("sql statement ="+sql);
	     	
	       	 targetCon.setAutoCommit(false);

		     st = DatabaseManager.getStatement(sourceCon);
		     resultSet = DatabaseManager.getResultSet(st, sql);
		    	
		    	sql2 = "insert into "+targetTable+"("+columns+") values ("+valueParams+")";
		    	ps = DatabaseManager.getPreparedStatement(targetCon, sql2);
		    	logger.debug("preparedstament="+sql2);
		    	
		    	int type=0;
		    	String colVal="";
		    	Map<String, String> newRowsUpdateMap=null;
		    	String colName="";
				
		    	if( newRowsUpdateValues != null && newRowsUpdateValues.size() > 0)
		    		newRowsUpdateMap = UtilityMethods.getMap(newRowsUpdateValues);
				logger.debug("new rows updatevalues= "+ newRowsUpdateMap);
				
				SimpleDateFormat formatter = new SimpleDateFormat("MM/dd/yyyy");
				java.util.Date parsed=null;

				while(resultSet.next())
		    	{
		    		
		    		for(int column=1; column<=count; column++)
		    		{
		                
		    			colName = resultSetMetaData.getColumnName(column);
		    			type = resultSetMetaData.getColumnType(column);
		    			colVal="";
		    			
		    			if((newRowsUpdateMap!=null && newRowsUpdateMap.size() > 0) && (newRowsUpdateMap.keySet().contains(colName)))
		    			{
		    				colVal = newRowsUpdateMap.get(colName);
		    			}
		    			
		    			if(type == -7 || type == -6)
		    		    {
	                        if(colVal=="")
	                        	ps.setShort(column, resultSet.getShort(column));
	                        else
	                        {
	                        	ps.setShort(column, Short.valueOf(colVal));
	                        }
		    		    } 
		    			else if(type == 4 || type == 5 || type == 2)
		    		    {
	                        if(colVal=="")
	                        	ps.setInt(column, resultSet.getInt(column));
	                        else
	                        {
	                        	ps.setInt(column, Integer.valueOf(colVal));
	                        }
		    		    }
		    		    else if(type == 12)
		    		    {
	                        if(colVal=="")
	                        	ps.setString(column, resultSet.getString(column));
	                        else 
	                        	ps.setString(column, colVal);
		    		    }
		    		    else if(type == 1 || type == 12 || type == -1 )
		    		    {
		    		    	if(colVal=="")
		    		    		ps.setCharacterStream(column, resultSet.getCharacterStream(column));
		    		    }
		    		    else if(type == 3)
		    		    {
		    		    	if(colVal == "")
		    		    		ps.setBigDecimal(column, resultSet.getBigDecimal(column));
		    		    	else
		    		    	{
		    		    		BigDecimal bdecimal = new BigDecimal(colVal);
		    		    		ps.setBigDecimal(column,bdecimal);	    		    		
		    		    	}	
		    		    }
		    		    else if(type == 8)
		    		    {
		    		    	if(colVal == "")
		    		    		ps.setDouble(column, resultSet.getDouble(column));
		    		    	else
		    		    		ps.setDouble(column, Double.valueOf(colVal));
		    		    }
		    		    else if(type == 6 || type == 7)
		    		    {
		    		    	if(colVal == "")
		    		    		ps.setFloat(column, resultSet.getFloat(column));
		    		    	else
		    		    		ps.setFloat(column, Float.valueOf(colVal));
		    		    }
		    		    else if(type == 91)
		    		    {
		    		    	
							if(colVal == "")
		    		    		ps.setDate(column, resultSet.getDate(column));
		    		    	else
		    		    	{	
		    		    	 parsed = formatter.parse(colVal);
		    			     java.sql.Date sqlDate = new java.sql.Date(parsed.getTime());
		    			     logger.debug("datevalueof = "+parsed);
		    			     ps.setDate(column, sqlDate);
		    		    	}
		    		    }
		    		    else if(type == 92)
		    		    {
		    		    	ps.setTime(column, resultSet.getTime(column));
		    		    }
		    		    else if(type == 2003)
		    		    {
		    		    	ps.setArray(column, resultSet.getArray(column));
		    		    }
		    		    else if(type == 2004)
		    		    {
		    		    	ps.setBlob(column, resultSet.getBlob(column));
		    		    }
		    		    else if(type == 2005)
		    		    {
		    		    	ps.setClob(column, resultSet.getClob(column));
		    		    }
		    		}
		    		ps.addBatch();rowCount++;
		    	}
		    	logger.debug("rows executed from sourcetable= "+rowCount);
		    	rowsCount = rowCount;
		    	
		    	int[] rc = ps.executeBatch();
			    logger.debug( targetTable +" rows inserted: "+ rc.length);
			    resultMap.put("sourceRows", Integer.toString(rowCount));
			    resultMap.put("targetRows", Integer.toString(rc.length));
			    resultMap.put("fromSql",sql);
			    resultMap.put("toSql", sql2);
			    resultMap.put("message", "ok");
			    
			    logger.debug("Thread: "+Thread.currentThread().getName()+"is running");
			    targetCon.commit();
	            }
		        catch (Exception e) {
		        	logger.error( "Threads failed!", e );
		        	if(e.getMessage().equals("1"))
		        		resultMap.put("message", "Error in query conditions");
		        	else
		        		resultMap.put("message", e.getMessage());
                    resultMap.put("sourceRows", Integer.toString(rowsCount));
        		    resultMap.put("targetRows", "0");
        		    resultMap.put("deletedRows", "0");
		        	try {
						targetCon.rollback();
					} catch (SQLException e1) {
						errorFalg = true;
						e1.printStackTrace();
					}		
			}
		    finally
		    {
	    	    	if(resultSet!=null)
		         		DatabaseManager.close(resultSet);
		    		if(st!=null)
		    			DatabaseManager.close(st);
		    		if(delSt!=null)
		    			DatabaseManager.close(st);
					if(ps!=null)
		    		    DatabaseManager.close(ps);
		    }
			return resultMap;
     }
}
