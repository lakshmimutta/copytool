/**
 * 
 */
package com.chrims.products.util;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;

import com.chrims.products.dao.DatabaseManager;
import com.chrims.products.model.CopyConditions;
import com.chrims.products.model.Rules;

/**
 * @author Annapurna
 *
 */
public class UtilityMethods {
	private static final Logger logger = Logger.getLogger(UtilityMethods.class); 

	public static String mapCondToSymbol(String condition, String value)
	{
		String symbol="";
		if(condition.equals("Greater than"))
			symbol = ">";
		else if(condition.equals("Less Than"))
			symbol = "<";
		else if(condition.equals("equal to"))
			symbol = "=";
		else if(condition.equals("is not null"))
			symbol = "is not null";
		else if(condition.equals("is null"))
			symbol = "is null";
		else if(condition.equals("not equal to"))
			symbol = "!=";
		else if(condition.equals("Like"))
			symbol = "like";
		else if(condition.equals("Date To From"))
			symbol = "between to_date("+value+",'%Y/%m/%d') and to_date("+value+",'%Y/%m/%d')";
		else if(condition.equals("between"))
		{
			String[] tokens = value.split(":");
			symbol = "between "+tokens[0]+" and "+tokens[1];
		}
		return symbol;
	}
	
	public static String newMapCondToSymbol(String condition, String value) 
	{
		String symbol="";
		if(condition.equals(">"))
			symbol = ">";
		else if(condition.equals("<="))
			symbol = "<=";
		else if(condition.equals(">="))
			symbol = ">=";
		else if(condition.equals("<"))
			symbol = "<";
		else if(condition.equals("="))
			symbol = "=";
		else if(condition.equals("is not null"))
			symbol = "is not null";
		else if(condition.equals("is null"))
			symbol = "is null";
		else if(condition.equals("!="))
			symbol = "!=";
		else if(condition.equals("Like"))
			symbol = "like";
		else if(condition.equals("between"))
		{
			String[] tokens = value.split(":");
			logger.debug("tokens ="+tokens);
			symbol = "between "+tokens[0]+" and "+tokens[1];
		}
		return symbol;
	}

	public static String getSetClause(List<CopyConditions> upRules)
	{
		String setClause="";
		String eachRule="";
		int count = 0;
		for(CopyConditions rule: upRules)
		{
			if((rule.getCondition().equals("Set DateTo")))
			{	
				eachRule= rule.getColumnName()+" = to_date('"+rule.getConditionValue()+"','%Y/%m/%d')";
			}	
			else if((rule.getCondition().equals("Active/InActive")))
			{	
	            String value = "";
	            if(rule.getConditionValue().equals("on"))
	              value ="t";
	            else
	              value="f";
				eachRule= "( "+rule.getColumnName()+" = "+value+" )";
			}
			else if(rule.getCondition().equals("="))
			{
				eachRule = rule.getColumnName()+" = "+rule.getConditionValue();
			}
			if(++count != upRules.size())
		    	setClause += eachRule+",";
		    else 
		    	setClause += eachRule;
		    eachRule="";
		}
		DatabaseManager.logger.debug("set Clause = "+setClause);
		return setClause;
	}

	public static String buildWhereClause(List<Rules> rules)
	{
		String whereCondition="";
		String eachRule="";
		int count = 0;
		for(Rules rule: rules)
		{
			if((rule.getCondition().equals("is null")) || (rule.getCondition().equals("is not null")))
			    eachRule= "( "+rule.getColumnName()+" "+mapCondToSymbol(rule.getCondition(), rule.getConditionValue())+" )";
			else if((rule.getCondition().equals("Date To From")))
				eachRule= "( "+rule.getColumnName()+" "+mapCondToSymbol(rule.getCondition(), rule.getConditionValue())+")";	
			else
				eachRule= "( "+rule.getColumnName()+" "+mapCondToSymbol(rule.getCondition(), rule.getConditionValue())+" "+rule.getConditionValue()+" )";
		    if(++count != rules.size())
		    	whereCondition += eachRule+" AND ";
		    else 
		    	whereCondition += eachRule;
		    eachRule="";
		}
		whereCondition = " where"+" "+whereCondition;
		DatabaseManager.logger.debug("where Condition = "+whereCondition);
		return whereCondition;
	}
	
	public static String buildWhereFromConditions(List<CopyConditions> rules) 
	{
		String whereCondition="";
		String eachRule="";
		int count = 0;
		
		for(CopyConditions rule: rules)
		{
			if((rule.getCondition().equals("is null")) || (rule.getCondition().equals("is not null")))
			    eachRule= "( "+rule.getColumnName()+" "+newMapCondToSymbol(rule.getCondition(), rule.getConditionValue())+" )";
			else if((rule.getCondition().equals("Date To From")) || (rule.getCondition().equals("between")))
				eachRule= "( "+rule.getColumnName()+" "+newMapCondToSymbol(rule.getCondition(), rule.getConditionValue())+")";
			else 
				eachRule= "( "+rule.getColumnName()+" "+newMapCondToSymbol(rule.getCondition(), rule.getConditionValue())+" "+rule.getConditionValue()+" )";
			
			if(++count != rules.size())
		    	whereCondition += eachRule+" AND ";
		    else 
		    	whereCondition += eachRule;
		    
			eachRule="";
		}
		whereCondition = " where"+" "+whereCondition;
		DatabaseManager.logger.debug("where Condition = "+whereCondition);
		return whereCondition;
	}


	public static Map<String,String> getMap(List<CopyConditions> rules)
	{
		Map<String,String> inRulesMap = new HashMap<String,String>();
		
			for(CopyConditions rule:rules)
			{
			   inRulesMap.put(rule.getColumnName(), rule.getConditionValue());
			}
			return inRulesMap;		
	 }
}
