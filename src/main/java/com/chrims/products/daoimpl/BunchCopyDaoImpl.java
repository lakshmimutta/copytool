/**
 * 
 */
package com.chrims.products.daoimpl;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

import javax.naming.Context;
import javax.naming.NamingException;
import javax.naming.ldap.InitialLdapContext;
import javax.naming.ldap.LdapContext;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.dao.InvalidDataAccessApiUsageException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;

import com.chrims.products.dao.BunchCopyDao;
import com.chrims.products.dao.BunchCopyDataAccess;
import com.chrims.products.model.CopyConditions;
import com.chrims.products.model.RulesListObject;
import com.chrims.products.model.UserDetails;
import com.chrims.products.util.CopyRecordsThread;
import com.chrims.products.util.UpdateRecordsThread;

/**
 * @author Annapurna
 *
 */

@Repository
public class BunchCopyDaoImpl implements BunchCopyDao{

	protected static final Log logger = LogFactory.getLog(BunchCopyDaoImpl.class);
	
    @Autowired
    private BunchCopyDataAccess dataAccess;

    @Autowired
    private UserDetails user;
   
	public List<RulesListObject> getRulesList() {
		String sql = "select * from rules_list";
		List<RulesListObject> rulesList = dataAccess.getJdbcTemplate("remote").query(sql, new RulesMapper());
		return rulesList;
	}

	public String insertRule(RulesListObject rule) {
		String result="Inserted record";
		String insertSql = "Insert into rules_list(rule_name,source_table,target_table,rules_list,delete_rows,update_conditions,newrows_updateval) values(?,?,?,?,?,?,?)";
		dataAccess.getJdbcTemplate("remote").update(insertSql);
		String ruleName,source,target,rulesList,deleteRows,updateConditions,newrowsUpdateVal;
		ruleName = rule.getRuleName();
		source = rule.getSourceTable();
		target = rule.getTargetTable();
		rulesList = rule.getRulesList();
		deleteRows = rule.getDeleteRows();
		updateConditions = rule.getUpdateRules();
		newrowsUpdateVal = rule.getNewUpRules();
		
		dataAccess.getJdbcTemplate("remote").update(insertSql,new Object[]{ruleName,source,target,rulesList,deleteRows,updateConditions,newrowsUpdateVal});
		return result;
	}
	private static final class RulesMapper implements RowMapper<RulesListObject>{

		public RulesListObject mapRow(ResultSet resultSet, int rowNum) throws SQLException {
			
			RulesListObject rule = new RulesListObject();
			rule.setRuleName(resultSet.getString("rule_name"));
			rule.setSourceTable(resultSet.getString("source_table"));
			rule.setTargetTable(resultSet.getString("target_table"));
			rule.setRulesList(resultSet.getString("rules_list"));
			rule.setDeleteRows(resultSet.getString("delete_rows"));
			rule.setUpdateRules(resultSet.getString("update_conditions"));
			rule.setNewUpRules(resultSet.getString("newrows_updateval"));
			
			return rule;			
		}
	}
	
	//this method performs the actual copy part from source to target based on selected conditions
	public Map<String,String> recordsUpdate(String sourceDB, String sTable,
			String targetDB, String tTable,
			List<CopyConditions> copyConditions, List<CopyConditions> newRowsUpdateVal) {
	
		logger.debug("logger reached recordsCopyMethod");
		Connection sourceCon,targetCon;
		if(sourceDB.equals("localDb"))
			sourceCon = dataAccess.getConnection("local");
		else if(sourceDB.equals("remoteDb")) 
			sourceCon= dataAccess.getConnection("remote");
		else //if(sourceDB.equals("SqlServerDb"))
		    sourceCon = dataAccess.getConnection("sqlserver");
		
		if(targetDB.equals("localDb"))
			targetCon = dataAccess.getConnection("local");
		else if(targetDB.equals("remoteDb"))
			targetCon = dataAccess.getConnection("remote");
		else //if(targetDB.equals("SqlServerDb"))
			targetCon = dataAccess.getConnection("sqlserver");
 
		JdbcTemplate bunchcopy = dataAccess.getJdbcTemplate("BunchCopy");
		
		Map<String, String> result = new HashMap<String,String>(); 
		
		ExecutorService executor = Executors.newCachedThreadPool();
		
		List<Future<Map<String, String>>> list = new ArrayList<Future<Map<String, String>>>();
		
		Callable<Map<String,String>> callable = new CopyRecordsThread(sourceCon,sTable,targetCon,tTable,copyConditions,newRowsUpdateVal);
		
		Future<Map<String, String>> future;
		for(int i=0; i<1; i++)
		{
			future = executor.submit(callable);
			list.add(future);
		}
		
		try { 
			result = list.get(0).get();
			logger.debug("results from copy records thread"+result);
		} catch (InterruptedException e) {
			 logger.error(e);
		} catch (ExecutionException e) {
			 logger.error(e);
		} catch (Exception e){
			logger.debug("Error in dao from threds");
		}
		
		if(result.get("message").equals("ok"))
		{
			String sql = "Insert into bunchcopy_log ("+"source_db,"+"target_db,"+"table_name,"+"time_of_action,"+"from_sql,"+"to_sql,"+"user_name,"+"rows_copied"+")"+"VALUES (?, ?, ?, ?, ?, ?, ?, ?)";
			Object[] params = new Object[] { sourceDB,targetDB,sTable,(new Date()).toString(),result.get("fromSql"),result.get("toSql"),user.getName(),result.get("targetRows")};
			int[] types = new int[] { Types.VARCHAR, Types.VARCHAR, Types.VARCHAR,Types.VARCHAR, Types.VARCHAR, Types.VARCHAR,Types.VARCHAR, Types.INTEGER };
			int row = bunchcopy.update(sql, params, types);
			logger.debug("Row entered into bunchcopylog"+row);
		}

		return result;
	}
	
	/* (non-Javadoc)
	 * @see com.chrims.products.dao.BunchCopyDao#targetRecordsUpdate(java.lang.String, java.lang.String, java.util.ArrayList, java.util.ArrayList)
	 */

	// this method updates the target table based on selected conditions for target
	public Map<String, String> targetRecordsUpdate(String targetDB,
			String tTable, ArrayList<CopyConditions> copyConditions,
			ArrayList<CopyConditions> updateValues) {
		logger.debug("logger reached recordsUpdateMethod");

		Connection targetCon;
		
		if(targetDB.equals("localDb"))
			targetCon = dataAccess.getConnection("local");
		else
			targetCon = dataAccess.getConnection("remote");
		
		Map<String, String> result = new HashMap<String,String>(); 
		
		ExecutorService executor = Executors.newFixedThreadPool(1);
		
		List<Future<Map<String, String>>> list = new ArrayList<Future<Map<String, String>>>();
		
		Callable<Map<String,String>> callable = new UpdateRecordsThread(targetCon,tTable,copyConditions,updateValues);
		
		Future<Map<String, String>> future;
		for(int i=0; i<1; i++)
		{
			future = executor.submit(callable);
			list.add(future);
		}
		try {
			result = list.get(0).get();
		} catch (InterruptedException e) {
			 logger.error(e);
		} catch (ExecutionException e) {
			 logger.error(e);
		}
		return result;
	}
	
	public Set<String> getTablesByRoleAccess()
	{
		List<String> roles = user.getRoleNames();
		List<String> tablesList = null;
		Set<String> tables = new HashSet<String>();
		
		String sql = "select table_name from role_table_permissions where role_name = ? and access_level in ('R')";

		for(String role: roles)
		{
			tablesList = (List<String>)dataAccess.getJdbcTemplate("BunchCopy").queryForList(sql, new Object[] {role}, String.class);
			for(String table: tablesList)
			{
				if(!tables.contains(table))
							tables.add(table);		
			}
			tablesList = null;
		}
	    return tables;
	}
	
	public void getUserDetails(String userName, String password) {
		logger.debug("before request reached getUserDetailsLdap()");
		if (authenticateUserLdap(userName, password)){

		logger.debug("authenticated user");
		logger.debug("after request reached getUserDetailsLdap()");
		Object args[] = {userName};
		UserDetails userDetails = new UserDetails();
		userDetails.setName(userName);
				
		String sql = "SELECT user_id FROM user_details WHERE user_name = ?";
		String junctionSql= "select role_id from user_role_table where user_id = ";
        String roleSql = "select role_name from role_details where role_id in (:ids)";
		
        Integer userId = null;
        List<Integer> roleIdList = null;
        List<String> roleNameList = null;
        
        try
        {
        	Object o = (Integer)dataAccess.getJdbcTemplate("BunchCopy").queryForObject(sql, args, Integer.class);
        	userId = (Integer)o;
        	logger.debug("userid ="+userId);
        }
        catch(EmptyResultDataAccessException e)
        {
        	logger.debug(e.getMessage());
        }
        
        if (userId!=null)
		{
           userDetails.setUserId(userId);
           
		  try{
        	roleIdList =dataAccess.getJdbcTemplate("BunchCopy").query(junctionSql+userId,  new RowMapper<Integer>() {
			      public Integer mapRow(ResultSet resultSet, int i) throws SQLException {
			          return resultSet.getInt(1);
			   }
		   });	
		  }
  	      catch(EmptyResultDataAccessException e)
		  {
			   logger.debug("no entries for the given id in user_role table");
			   logger.debug(e.getMessage());
			   roleIdList = null;
		  }
		}
        logger.debug("ids = "+roleIdList);
		
        if(roleIdList != null && roleIdList.size() > 0)
		{
        	userDetails.setRoleIds(roleIdList);
    		
			Map<String, List<Integer>> paramMap = Collections.singletonMap("ids",roleIdList);
			NamedParameterJdbcTemplate  namedParameterJdbcTemplate = new  
					NamedParameterJdbcTemplate(dataAccess.getJdbcTemplate("BunchCopy"));
			try{
				roleNameList =  namedParameterJdbcTemplate.queryForList(roleSql, paramMap, String.class);	
			  }
			catch(InvalidDataAccessApiUsageException e)
			{
				logger.debug(e.getMessage());
				roleNameList = null;
			}
		}

        userDetails.setRoleNames(roleNameList);
        userDetails.setName(userName);
        
		logger.debug("rolesNamesList ="+roleNameList);
		logger.debug("userDetails from dao ="+userDetails);
		user.addUserValues(userDetails);
		}
		else
		{
			user.setUserId(0);
		}
	}
	private boolean authenticateUserLdap(String userName, String password) {
		boolean validUser=true;
		 
		final String ldapAdServer = "ldap://172.16.40.100:389";
   //     final String ldapSearchBase = "dc=chrims,dc=com";
        
        final String ldapUsername = userName;
        final String ldapPassword = password;
        
		Hashtable env = new Hashtable();
        env.put(Context.INITIAL_CONTEXT_FACTORY,"com.sun.jndi.ldap.LdapCtxFactory");
        
        //set security credentials
        env.put(Context.SECURITY_AUTHENTICATION,"simple");
        env.put(Context.SECURITY_PRINCIPAL,ldapUsername);
        env.put(Context.SECURITY_CREDENTIALS,ldapPassword);
    	
        //connect to my domain controller
        env.put(Context.PROVIDER_URL, ldapAdServer);
        env.put("com.sun.jndi.ldap.trace.ber", System.err);
        LdapContext ctx = null;
        try {
   	     // Create the initial directory context
        	ctx = new InitialLdapContext(env,null);
        	logger.debug("connection established "+ctx+" i.e valid user");
        }
        catch(NamingException ex)
        {
        	logger.debug("unidentified user "+ ex);
        	validUser=false;
        }
        finally
        {
        	if(ctx!=null)
				try {
					ctx.close();
				} catch (NamingException e) {
					e.printStackTrace();
				}
        }
        return validUser;
	}
}
