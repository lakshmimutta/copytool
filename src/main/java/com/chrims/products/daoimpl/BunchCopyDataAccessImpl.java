/**
 *
 */
package com.chrims.products.daoimpl;

import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.SQLException;

import javax.sql.DataSource;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import com.chrims.products.dao.BunchCopyDataAccess;

/**
 * @author Annapurna
 *
 */
@Repository
public class BunchCopyDataAccessImpl implements BunchCopyDataAccess
{
	private static final Logger logger = Logger.getLogger(BunchCopyDataAccessImpl.class);
    @Autowired
    @Qualifier("localDataSource")
	private DataSource localDataSource;
    
    @Autowired
    @Qualifier("remoteDataSource")
    private DataSource remoteDatasource;

    @Autowired
    @Qualifier("sqlServerDataSource")
    private DataSource sqlServerDataSource;
    
    @Autowired
    @Qualifier("bunchCopyDataSource")
    private DataSource bunchCopyDataSource;
    
    private Connection localConn,remoteConn,sqlServerConn,bunchcopyConn;
    
    @Autowired
    @Qualifier("jdbcTemplateBunchCopy")
	private JdbcTemplate bunchCopyTemplate;
    
    @Autowired
    @Qualifier("jdbcTemplateRemote")
	private JdbcTemplate jdbcTemplateRemote;
    
    @Autowired
    @Qualifier("jdbcTemplateLocal")
	private JdbcTemplate jdbcTemplateLocal;
    
    @Autowired
    @Qualifier("jdbcTemplateSqlServer")
	private JdbcTemplate jdbcTemplateSqlServer;
	
    public DataSource getLocalDataSource() {
		return localDataSource;
	}

	public void setLocalDataSource(DataSource localDataSource) {
		this.localDataSource = localDataSource;
	}

	public DataSource getRemoteDatasource() {
		return remoteDatasource;
	}
	
	public void setRemoteDatasource(DataSource remoteDatasource) {
		this.remoteDatasource = remoteDatasource;
	}
		
    public DataSource getSqlServerDataSource() {
		return sqlServerDataSource;
	}

	public void setSqlServerDataSource(DataSource sqlServerDataSource) {
		this.sqlServerDataSource = sqlServerDataSource;
	}

	public Connection getConnection(String db) {
    	Connection con=null;
		
		try {
			if(db.equals("remote"))
			{	
				remoteConn = getRemoteDatasource().getConnection();
				con =  remoteConn;
			}
			else if(db.equals("local"))
			{	
				localConn = getLocalDataSource().getConnection();
				con = localConn;
			}
			else if(db.equals("sqlserver"))
			{	
				sqlServerConn = getSqlServerDataSource().getConnection();
				con = sqlServerConn;
			}
			else if(db.equals("BunchCopy"))
			{
				bunchcopyConn = getBunchCopyDataSource().getConnection();
				con = bunchcopyConn;
			}
			
		} catch (SQLException e) {
			logger.error(e.getMessage(), e);
		}
		logger.debug("request reached getConnection ,connection= "+con);
		return con;
	}

	public DataSource getBunchCopyDataSource() {
		return bunchCopyDataSource;
	}

	public DatabaseMetaData getDatabaseMetaData(String db)
	{
		DatabaseMetaData metadata=null; 
		try {
			 if(db.equals("local"))
			 { 
				if(localConn==null)
					getConnection("local");
				metadata = localConn.getMetaData();
			 }
			 else if(db.equals("remote"))
			 {
				if(remoteConn==null)
					getConnection("remote");
				    metadata = remoteConn.getMetaData();
			 }	 
			 else if(db.equals("sqlserver"))
			 {
				 if(sqlServerConn == null)
					 getConnection("sqlserver");
				     metadata = sqlServerConn.getMetaData();
			 }
			} 
		    catch (SQLException e) {			
				e.printStackTrace();
			}
		logger.debug("request reached getDatabaseMetadata, dbmetadata = "+ metadata);
		return metadata;
	}

	public JdbcTemplate getJdbcTemplate(String db) {
   	    JdbcTemplate jdbcTemplate = null; 
        if(db == "local")
        	jdbcTemplate = jdbcTemplateLocal;
        else if(db == "remote")
        	jdbcTemplate= jdbcTemplateRemote;
        else if(db == "BunchCopy")
        	jdbcTemplate = bunchCopyTemplate;
        else if(db == "sqlserver")
        	jdbcTemplate = jdbcTemplateSqlServer;
        return jdbcTemplate;
	}
}


